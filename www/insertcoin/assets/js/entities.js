import {loadMario} from 'http://localhost:8181/backend/assets/js/entities/Mario.js';
import {loadGoomba} from 'http://localhost:8181/backend/assets/js/entities/Goomba.js';
import {loadKoopa} from 'http://localhost:8181/backend/assets/js/entities/Koopa.js';


export function loadEntities() {
    const entityFactories = {};

    function addAs(name) {
        return factory => entityFactories[name] = factory;
    }


    return Promise.all([
        loadMario().then(addAs('mario')),
        loadGoomba().then(addAs('goomba')),
        loadKoopa().then(addAs('koopa')),
    ])
    .then(() => entityFactories);
}
