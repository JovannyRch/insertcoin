<div class="container-fluid" style="padding-left: 5%;padding-right: 5%" id="app">
    <div class="row animated bounceIn" v-show="!cargandoJuego">


        <div class="card col s12  orange lighten-5">
           
                <div class="col s2 center " style="height: 55em; margin-top:3%" >
                        <div class="col s12">
                            <img :src="juego.IMG" width="50%" class="left">
                            <b>{{juego.CLASIFICACION}}</b>
                            <br>
                            <small>Creado por: <a
                                    :href="`index.php/inicio/usuario/${juego.CREADOR}`">{{juego.CREADOR}}</a></small>
                        </div>
                        <div class="col s12">
                            <h4 class="left blue-grey-text">{{puntos}}</h4>
                            <h4 :class="tiempo > 30? 'black-text right': 'red-text right'">
                                <i class="fas fa-clock black-text"></i>
                                {{fomatearTiempo}}</h4>
                        </div>
        
                        <div class="col s12  left-align" style="margin-bottom:5px">
                            <h6>Record <span class="right"><b>{{jugadoActual.RECORD}}</b></span></h6>
                            <h6>Promedio <span class="right"><b> {{jugadoActual.PROMEDIO}}</b></span></h6>
        
                        </div>
                        <div style="min-height: 45em">
                            <div v-for="(h,index) in historial" class="col s2 center-align center">
        
                                <a class="btn-floating btn-small green" v-if="h">
                                    <i class="fa fa-check"> </i>
                                </a>
                                <a class="btn-floating btn-small red" v-else>
                                    <i class="fa fa-times"> </i>
        
                                </a>
                                <br> <span>{{index+1}}</span>
                            </div>
                        </div>
                        <div class="left-align">
                            <a  :href="`index.php/inicio/juego/${juego_id}/global`" class="btn black-text orange white-text">Clasificación Global</a>
                            <a style="margin-top:3%" :href="`index.php/inicio/juego/${juego_id}/stats/${current_user_id}`" class="btn black-text orange white-text">Mis estadísticas</a>
                        </div>
                    </div>

            <div class="card-content col s8  orange lighten-5"
                style="height: 55em; border-right: 10px solid #ffffff;  border-left: 10px solid #ffffff">
                <div class="col s12 ">

                    <div class="col s12 center orange lighten-4">
                        <h5><b>{{juego.NOMBRE}}</b></h5>
                    </div>

                </div>
                <div class="center " v-if="pagina == 0">
                    <div style="margin-top: 25%">
                        <a @click="iniciarJuego()"
                            class="btn-floating btn-large waves-effect waves-light orange animated pulse infinite"><i
                                class="fas fa-play"></i></a>
                        <br>
                        <b class="orange-text">Iniciar</b>
                    </div>
                </div>
                <div v-if="pagina == 1" class="center white animated bounceIn">

                    <div class="col s9" style="margin-top: 15%; margin-left: 15%">
                        <div v-if="jugando">
                            <h3 class="deep-orange-text">{{preguntaActual.CUERPO}}</h3>
                            <img v-show="preguntaActual.IMG" :src="preguntaActual.IMG" alt="" width="30%"
                                style="margin-bottom:5%">
                                <button @click="contestar(respuesta[1])" style="font-size: 38px; margin-left:3%;margin-top:2%;"
                                    v-for="respuesta in preguntaActual.RESPUESTAS" style="margin-right:5em"
                                    class="btn waves-effect waves-light  orange white-text">
                                    {{respuesta[0]}}</button>
                        </div>
                        <div v-else>
                            <h3 v-if="nuevoRecord" class="orange-text animated pulse infinite">
                                ¡Tu nuevo record es {{puntos}}!
                            </h3>
                            <h3 v-else class="orange-text">
                                Tu puntuacion {{puntos}}
                            </h3>
                            <br><br>
                            <a @click="jugarOtraVez()"
                                class="btn-floating btn-large waves-effect waves-light deep-orange animated pulse infinite"><i
                                    class="fas fa-redo"></i></a>
                            <br>
                            <b class="deep-orange-text">Volver a jugar</b>

                        </div>

                    </div>



                </div>

            </div>
            <div class="col s2" style="height: 55em;">
                <h3 class="center  amber-text darken-3"><i class="fas fa-trophy"></i></h3>
                <!--  <ul class="collection ">
                    <li class="collection-item avatar  orange lighten-5" v-for="jugador in jugadoActual.TOP">
                        <img :src="jugador.AVATAR" alt="" class="circle">
                        <span class="title">{{jugador.USERNAME}}</span>
                       
                        <div class="secondary-content">
                                {{jugador.PUNTUACION}}
                        </div>
                    </li>
                </ul>-->
                <table>
                    <thead>
                        <th style="width: 5%"></th>
                        <th style="width: 15%"></th>
                        <th>Clasificación</th>
                        <th style="width: 5%"></th>
                    </thead>
                    <tbody>
                        <tr v-for="(jugador,index) in jugadoActual.TOP">
                            <td>
                                {{index+1}}
                            </td>
                            <td>
                                <img :src="jugador.AVATAR" width="100%">
                            </td>
                            <td>
                                <a :href="`index.php/inicio/juego/${juego_id}/stats/${jugador.ID}`">{{jugador.USERNAME}}</a>
                            </td>
                            <td>
                                <b>{{jugador.PUNTUACION}}</b>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
          

        </div>
    </div>
    <div v-show="cargandoJuego">
        <div class="progress">
            <div class="indeterminate"></div>
        </div>

    </div>

</div>

<script>
    document.addEventListener('DOMContentLoaded', function () {
        var elems = document.querySelectorAll('.modal');
        var instances = M.Modal.init(elems, {});
    });

    const app = new Vue({
        el: "#app",
        data: {
            current_user_id: "<?=$this->session->userdata('ID')?>",
            juego: {
                NOMBRE: '',
                CREADOR: ''
            },
            segundos: 180,
            cargandoJuego: false,
            juego_id: "<?=$juego_id?>",
            jugando: false,
            tiempo: 180, //  3 min
            errores: 0,
            preguntaActual: {
                "ID": "",
                "CUERPO": "",
                "IMG": '',
                "NIVEL_ID": "",
                "RESPUESTAS": ""
            },
            nivelActualIndex: 0,
            preguntaActualIndex: -1,
            pagina: 0,
            historial: [],
            jugadoActual: {
                "AVATAR": "",
                "USERNAME": "",
                "IMG": "",
                "RECORD": '',
                "PROMEDIO": '',
                "ULTIMOSINTENTOS": [],
                "TOP": []
            },
            puntos: 0,
            nuevoRecord: false,
            coin: new Audio(),
            errorAudio: new Audio(),
            correctAudio: new Audio(),
            animaciones: [
                'pulse',
                'rubberBand',
                'shake',
                'swing'
            ],
            verRespuestas: false

        },
        created: function () {
            this.cargarDatosJugador();
            this.cargarJuego();
            this.cronometro();
            this.coin.src = "assets/coin7.wav";
            this.errorAudio.src = "assets/error.wav";
            this.correctAudio.src = "assets/correct.mp3";

        },
        methods: {

            cargarJuego() {
                this.cargandoJuego = true;
                axios.get("index.php/api/juegoPlayer/" + this.juego_id).then(
                    juego => {
                        this.juego = juego.data;

                        for (i in this.juego.NIVELES) {
                            for (j in this.juego.NIVELES[i].PREGUNTAS) {
                                this.juego.NIVELES[i].PREGUNTAS[j].RESPUESTAS = this.toJSON(this.juego
                                    .NIVELES[i].PREGUNTAS[j].RESPUESTAS);
                            }
                        }



                        this.cargandoJuego = false;
                    }
                )
            },
            cronometro() {
                setInterval(function () {
                    console.log(app.jugando);
                    if (app.jugando) {
                        app.tiempo--;
                    }
                    if (app.tiempo == 0 && app.jugando) {
                        //Terminar juego
                        app.terminarJuego();
                    }
                }, 1000);
            },
            iniciarJuego() {
                this.coin.play();
                this.puntos = 0;
                this.tiempo = this.segundos;

                this.jugando = true;
                this.pagina = 1;

                this.nuevoRecord = false;
                this.preguntaActualIndex = -1;
                this.nivelActualIndex = 0;
                this.errores = 0;
                this.historial = [];
                this.siguientePregunta();
            },
            jugarOtraVez() {
                this.coin.play();
                this.tiempo = this.segundos;


                this.nuevoRecord = false;
                this.preguntaActualIndex = -1;
                this.nivelActualIndex = 0;
                this.errores = 0;
                this.historial = [];
                this.solicitarNuevasPreguntas();

                this.puntos = 0;
            },
            solicitarNuevasPreguntas() {
                axios.get("index.php/api/juegoPlayer/" + this.juego_id).then(
                    juego => {
                        this.juego = juego.data;

                        for (i in this.juego.NIVELES) {
                            for (j in this.juego.NIVELES[i].PREGUNTAS) {
                                this.juego.NIVELES[i].PREGUNTAS[j].RESPUESTAS = this.toJSON(this.juego
                                    .NIVELES[i].PREGUNTAS[j].RESPUESTAS);
                            }
                        }
                        this.jugando = true;
                        this.siguientePregunta();



                    }
                )
            },
            siguientePregunta() {
                this.verRespuestas = false;
                if (this.jugando) {
                    this.preguntaActualIndex += 1;
                    let cantPreguntas = this.juego.NIVELES[this.nivelActualIndex].PREGUNTAS.length;
                    let cantNiveles = this.juego.NIVELES.length;
                    if (this.preguntaActualIndex >= cantPreguntas) {
                        this.nivelActualIndex += 1;
                        this.preguntaActualIndex = 0;
                    }
                    if (this.nivelActualIndex >= cantNiveles) {
                        this.terminarJuego();
                    } else {

                        this.preguntaActual = this.juego.NIVELES[this.nivelActualIndex].PREGUNTAS[this
                            .preguntaActualIndex];
                    }
                }
            },
            terminarJuego() {
                this.jugando = false;
                //Verificar si es nuevo record


                if (this.puntos > parseInt(this.jugadoActual.RECORD)) {
                    this.nuevoRecord = true;
                }

                //Guardar intento
                datos = {
                    juego_id: this.juego_id,
                    jugador_id: this.current_user_id,
                    puntuacion: this.puntos
                };

                axios.post('index.php/api/guardarIntento', datos).then(
                    response => this.cargarDatosJugador()
                );

            },
            formatearString(cadena) {
                let resultado = "";
                for (letra of cadena) {
                    if (letra == "'") {
                        resultado += '"'
                    } else {
                        resultado += letra;
                    }
                }
                return resultado;
            },
            toJSON(cadena) {
                objeto = this.shuffle(JSON.parse(this.formatearString(cadena)));
                return objeto;
            },
            shuffle(a) {
                for (let i = a.length - 1; i > 0; i--) {
                    const j = Math.floor(Math.random() * (i + 1));
                    [a[i], a[j]] = [a[j], a[i]];
                }
                return a;
            },
            contestar(respuesta) {
                this.verRespuestas = false;
                this.historial.push(respuesta);
                //Guarda resultado de la pregunta en la BD

                if (respuesta) {
                    this.puntos += 1;
                    this.guardarRespuesta(1);
                    this.correctAudio.play();
                } else {
                    this.errores += 1;
                    this.guardarRespuesta(0);
                    this.errorAudio.play();

                    if (this.errores == 3) {
                        this.terminarJuego();
                        //Guardar intento del jugador
                    }
                }
                this.siguientePregunta();
            },
            guardarRespuesta(respuesta) {
                axios.put(`index.php/api/guardarResultado/${this.preguntaActual.ID}/${respuesta}`);
            },
            cargarDatosJugador() {
                axios.get(`index.php/api/jugadorActual/${this.current_user_id}/${this.juego_id}`).then(
                    response => {
                        this.jugadoActual = response.data;
                        this.graficar1();
                    }
                )
            },
            graficar1() {

                Highcharts.chart('grafica1', {

                    title: {
                        text: 'Últimos 5 intentos'
                    },

                    yAxis: {
                        title: {
                            text: 'Puntuación'
                        }
                    },
                    legend: {
                        layout: 'vertical',
                        align: 'right',
                        verticalAlign: 'middle'
                    },

                    plotOptions: {
                        series: {
                            label: {
                                connectorAllowed: true
                            },
                            pointStart: 1
                        }
                    },

                    series: [{
                        name: 'Puntuación',
                        data: app.getUltimasPuntuaciones()
                    }],


                    responsive: {
                        rules: [{
                            condition: {
                                maxWidth: 500
                            },
                            chartOptions: {
                                legend: {
                                    align: 'center',
                                    verticalAlign: 'bottom',
                                    layout: 'horizontal'
                                },
                                yAxis: {
                                    labels: {
                                        align: 'left',
                                        x: 0,
                                        y: -5
                                    },
                                    title: {
                                        text: null
                                    }
                                },
                                subtitle: {
                                    text: null
                                },
                                credits: {
                                    enabled: false
                                }
                            }
                        }]
                    }

                });
            },
            getUltimasPuntuaciones() {
                let resultado = [];
                for (p of this.jugadoActual.ULTIMOSINTENTOS) {
                    resultado.push(parseInt(p.PUNTUACION));
                }
                return resultado;
            }
        },
        computed: {

            fomatearTiempo() {
                minutos = parseInt(this.tiempo / 60);
                segundos = this.tiempo % 60;

                return segundos > 9 ? '0' + minutos + " : " + segundos : '0' + minutos + " : 0" + segundos;
            }
        }
    });
</script>