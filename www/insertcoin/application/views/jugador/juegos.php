<div class="container" id="app">
    <div class="row">


        <div class="col s12 m7">
            <h4>Quiz Mode</h4>

            <ul class="collapsible">
                <li v-for="clasificacion in clasificaciones">
                    <div class="collapsible-header">
                            <h6>{{clasificacion.NOMBRE}}</h6></div>
                    <div class="collapsible-body">
                        <div class="row">
                            <div class="col s4">
                                <div class="card">
                                    <div class="card-content">

                                        <div class="row">
                                            <img :src="clasificacion.IMG" class="col s12 center">
                                            <div class="cart-title col s12">
                                                <p class="center-align">
                                                    {{clasificacion.DESCRIPCION}}
                                                </p>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                                <div class="col s8">
                                        <ul class="collection">
                                                <li class="collection-item avatar" v-for="quiz in clasificacion.JUEGOS">
                                                    <img :src="quiz.IMG" alt="" class="circle">
                                                    <span class="title"><b>{{quiz.NOMBRE}}</b></span>
                                                    
                                                    <div class="secondary-content">
                                                        <a :href="`index.php/inicio/juego/${quiz.ID}`"><i class="fa fa-play"></i> Jugar </a>
                                                    </div>
                                                </li>
                                            </ul>
                                </div>
                            </div>
                        </div>
                </li>

            </ul>
        </div>
        <div class="col s12 m5">
            <h4>Juegos</h4>
            <ul class="collection">
                <li class="collection-item avatar" v-for="quiz in juegos">
                    <img :src="quiz.IMG" alt="" class="circle">
                    <span class="title"><b>{{quiz.NOMBRE}}</b></span>
                    
                    <div class="secondary-content">
                        <a href="index.php/games"><i class="fa fa-play"></i> Jugar </a>
                    </div>
                </li>
            </ul>
        </div>


    </div>

</div>

<script>
    document.addEventListener('DOMContentLoaded', function () {
        var elems = document.querySelectorAll('.modal');
        var instances = M.Modal.init(elems, {});
    });

    document.addEventListener('DOMContentLoaded', function () {
        var elems = document.querySelectorAll('.collapsible');
        var instances = M.Collapsible.init(elems, {});
    });



    let app = new Vue({
        el: "#app",
        data: {
            current_user_id: "<?=$this->session->userdata('ID')?>",
            quizes: [],
            clasificaciones: [],
            juegos: []
        },
        created: function () {
            this.cargarQuizes();
            this.cargarJuegos();
        },
        methods: {
            cargarQuizes() {
                axios.get('index.php/api/juegosXclasificaciones').then(
                    result => {
                        this.clasificaciones = result.data;
                    }
                );
            },
            cargarJuegos() {
                axios.get('index.php/api/table/juegos0').then(
                    result => {
                        this.juegos = result.data;
                    }
                );
            }
        }
    });
</script>