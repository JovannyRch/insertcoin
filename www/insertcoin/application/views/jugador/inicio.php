<div class="container" id="app">
    <div class="row">
        <h2>Publicaciones</h2>
        <ul class="collection">
            <li class="collection-item avatar" v-for="publicacion in publicaciones" >
                <img :src="publicacion.IMG" alt="" class="circle">
                <span class="title"><b>{{publicacion.TITULO}}</b></span>
                <br>
                {{publicacion.FECHA}}
                </p>
                <div class="secondary-content">

                    
                    <a class="btn btn-small transparent green-text" :href="`index.php/inicio/publicacion/${publicacion.ID}`">
                        ver
                    </a>

                </div>

            </li>

        </ul>
    </div>
</div>

<script>
    document.addEventListener('DOMContentLoaded', function () {
        var elems = document.querySelectorAll('.modal');
        var instances = M.Modal.init(elems, {});
    });


    let app = new Vue({
        el: "#app",
        created: function () {
            this.cargarPublicaciones();
        },
        data: {
            publicaciones: [],
            publicacion: {
                TITULO: '',
                CUERPO: '',
                IMG: ''
            },
            editar: false
        },
        methods: {
            cargarPublicaciones() {
                axios.get('index.php/api/table/postadmin').then(
                    response => this.publicaciones = response.data,
                    error => console.log('Error al cargar las publicaciones')
                )
            },
            clonarPost(publicacion) {
                this.publicacion = Object.assign({}, publicacion);
                this.editar = true;
            },
            resetPost() {
                this.editar = false;
                this.publicacion = {
                    TITULO: '',
                    CUERPO: '',
                    IMG: ''
                }
            }
        }
    });
</script>