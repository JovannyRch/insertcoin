<div class="container" id="app">
    <div class="row" v-if="!cargando">
        <div class="col s12 right">

            <div class="input-field col s3 right">
                <i class="fa fa-search prefix"></i>
                <input id="icon_prefix" type="text" class="validate" placeholder="Buscador..." v-model="buscador">

            </div>
            <div class="col s3 input-field right">

                <select v-model="buscarPor" class=" browser-default right">
                    <option value="" selected disabled>Buscar por</option>
                    <option value="1">Nombre de usuario</option>
                    <option value="2">Descripción</option>
                    <option value="3">País</option>
                </select>
            </div>
        </div>
        <table>
            <thead>
                <th style="width:2%">
                    #
                </th>
                <th style="width:4%">

                </th>
                <th style="width:20%">
                    Usuario
                </th>
                <th style="width:70%">
                    Descripción
                </th>
                <th style="width:4%">
                    País
                </th>
            </thead>
            <tbody>
                <tr v-for="(jugador,index) in filtrar">
                    <td>{{index+1}}</td>
                    <td>
                        <img :src="jugador.AVATAR" alt="" width="100%">
                    </td>
                    <td><a :href="`index.php/inicio/usuario/${jugador.USERNAME}`">{{jugador.USERNAME}}</a></td>
                    <td>{{jugador.BIO}}</td>
                    <td>
                        <img :src="jugador.IMG" alt="" width="100%">

                    </td>
                </tr>
            </tbody>
        </table>

    </div>
    <div v-else>
        <div class="progress">
            <div class="indeterminate"></div>
        </div>
    </div>

</div>

<script>
    document.addEventListener('DOMContentLoaded', function () {
        var elems = document.querySelectorAll('.modal');
        var instances = M.Modal.init(elems, {});
    });

    document.addEventListener('DOMContentLoaded', function () {
        var elems = document.querySelectorAll('select');
        var instances = M.FormSelect.init(elems, {});
    });


    let app = new Vue({
        el: "#app",
        data: {
            current_user_id: "<?=$this->session->userdata('ID')?>",
            jugadores: [],
            cargando: true,
            buscarPor: 1,
            buscador: ''
        },
        created: function () {
            this.cargarJugadores()
        },
        methods: {
            cargarJugadores() {
                this.cargando = true;
                axios.get('index.php/api/table/jugadores').then(
                    response => {
                        this.jugadores = response.data;
                        for(i in this.jugadores){
                            if(!this.jugadores[i].BIO) this.jugadores[i].BIO = "";
                        }
                        this.cargando = false;
                    }
                ).catch(error => {
                    this.cargando = false;
                })
            }
        },
        computed: {
            filtrar: function () {
                if (this.buscador === '') {
                    return this.jugadores;
                } else {

                    if (this.buscarPor == 1) {
                        return this.jugadores.filter((jugador) => {
                            return jugador.USERNAME.toString().toLowerCase().includes(this
                                .buscador.toLowerCase());
                        });
                    }
                    if (this.buscarPor == 2) {
                        
                        return this.jugadores.filter((jugador) => {
                            
                            return jugador.BIO.toString().toLowerCase().includes(this
                                .buscador.toLowerCase());
                        });
                    }
                    if (this.buscarPor == 3) {
                        return this.jugadores.filter((jugador) => {
                            return jugador.NOMBRE.toString().toLowerCase().includes(this
                                .buscador.toLowerCase());
                        });
                    }
                }
            }
        }
    });
</script>