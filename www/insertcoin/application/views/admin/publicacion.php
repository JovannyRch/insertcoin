<div id="app" class="container">

    <div class="row">

        <div class="card white" style="padding-bottom:8%">
            <div class="card-content">
                <h4 class="center-align "> {{publicacion.TITULO}} </h4>
                <div v-if="publicacion.IMG" class="center-align">
                    <img class="responsive-img" :src="publicacion.IMG" width="50%">
                </div>
                <div class="col s12 center-align" v-html="publicacion.CUERPO">

                </div>
            </div>
        </div>
        <div class="col s12" >
                <br>
                <br><br>
                <br>
            <h6><b>Comentarios</b></h6>


            <textarea class="materialize-textarea" placeholder="Escribe tu comentario" v-model="comentario.CONTENIDO">
                </textarea>
            <button class="btn deep-orange lighten-2" @click="publicarComentario"
                v-if="comentario.CONTENIDO">Comentar</button>

            <div v-if="publicacion.comentarios.length " style="border: solid 1px black">
                <ul class="collection">
                    <li class="collection-item avatar" v-for="(comentario,index) in publicacion.comentarios">
                        <img :src="comentario.AVATAR" alt="" class="circle">
                        <span class="title"> <a
                                :href="`index.php/inicio/usuario/${comentario.USUARIO}`">{{comentario.USUARIO}}
                            </a></span> <small class="grey-text lighten-2">{{comentario.FECHA}}</small>
                        <p> {{comentario.CONTENIDO}}
                        </p>
                        <a @click="eliminarComentario(comentario.COMENTARIO_ID, index)"
                            class="secondary-content btn-floating-text btn-small transparent"> <i
                                class=" black-text  fa fa-trash"></i> </a>
                    </li>
                </ul>
            </div>

            <br>
            <br>
            <br>
            <br>
            <br>
            <br>

            <span v-else class="center-align">
                <small>No se encontraron comentarios</small>
            </span>
        </div>
    </div>
</div>

<script>
    const app = new Vue({
        el: '#app',
        data: {
            publicacion_id: '<?=$id ?>',
            publicacion: {
                ID: '',
                TITULO: '',
                CUERPO: '',
                IMG: '',
                comentarios: []
            },
            comentario: {
                CONTENIDO: '',
                POST_ID: '<?=$id ?>',
                AUTOR_ID: "<?=$this->session->userdata('ID')?>"
            }
        },
        created: function () {
            this.cargarPublicacion()
        },
        methods: {
            cargarPublicacion() {
                axios.get('index.php/api/post1/' + this.publicacion_id).then(
                    result => this.publicacion = result.data,
                    error => alert('Error al cargar la publicacion')
                )
            },
            eliminarComentario(comentario_id, index) {
                if (confirm('¿Estás seguro de elimar tu comentario?')) {
                    axios.delete('index.php/api/table/comentarios_post_admin/' + comentario_id).then(
                        response => {
                            this.publicacion.comentarios.splice(index, 1);
                        }, error => console.log('Error al elinar el comentario')
                    )
                }
            },
            publicarComentario() {
                axios.post('index.php/api/table/comentarios_post_admin', {
                    data: this.comentario
                }).then(
                    response => {
                        this.resetComentario();
                        this.cargarPublicacion();
                    },
                    error => alert('Ocurrio un error al publicar el comentario')
                )
            },
            resetComentario() {
                this.comentario = {
                    CONTENIDO: '',
                    POST_ID: '<?=$id ?>',
                    AUTOR_ID: "<?=$this->session->userdata('ID')?>"
                }
            }
        }
    })
</script>