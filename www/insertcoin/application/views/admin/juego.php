<div class="container-fluid" id="app">
    <div class="row" style="margin-left:3%">
        <span class="deep-orange-text">MENU</span> <br>
        <ul class="collapsible col s2 collection">


           
            

            <li class="collection-item">
                    <div class="center">
                        <i class="fa fa-arrow-left"></i> <a href="index.php/inicio/juegos" class="black-text" >Regresar a clasificaciones</a>
                    </div>
                </li>
                <li @click="pagina = 2" class="collection-item">
                        <div class="center">
                            <i class="fas fa-chart-line"></i> Estadisticas del juego
                        </div>
                    </li>

                <li class="collection-item">
                        <div class="center">
                                <i class="fas fa-dice"></i> Niveles <br>
                        </div>
                        <small><i>Seleccione nivel</i></small> <br>
                         <ul v-if="juego.NIVELES.length" class=" collection">
                                 <li @click="seleccionarNivel(nivel)"  class="collection-item"  v-for="nivel in juego.NIVELES"> <i class="fas fa-angle-right"></i> {{nivel.NOMBRE}}</li>
                             </ul>
                             <div v-else>
                                 <i><small>Sin niveles</small></i>
                             </div>
                    </li>

        </ul>
        <div class="col s10 ">

            <div class="col s12">
                    <h4 class="center ">{{juego.NOMBRE}}</h4>

                    <div class="right">
                        <a href="#nivelForm" class="btn-small transparent black-text modal-trigger"
                            @click="nivelCopia.NOMBRE = ''"> <i class="fa fa-plus"></i>
                            Agregar
                            nivel
                        </a>
        
                        <button @click="eliminarNivel()" class="btn-small transparent black-text" v-if="nivel.NOMBRE"> <i class="fa fa-trash"></i> Borrar
                            nivel</button>
                        <a v-if="nivel.NOMBRE" class="btn-small transparent black-text modal-trigger" href="#nivelForm"
                            @click="editarNivel = true; edicionNivel();"> <i class="fa fa-pen"></i> Cambiar
                            nombre
                            de nivel</a>
        
                    </div>
            </div>
            <div v-if="pagina == 1" class="row">
                
               
                <div v-if="nivel.NOMBRE" style="margin-left:3%" class="col s11">
                    <h4><b>Nivel: <i>{{nivel.NOMBRE}}</i></b></h4>
                    <div v-if="!cargandoPreguntas">

                        <div class="right">

                            <a class="modal-trigger btn-small transparent black-text" href="#preguntaForm"
                                @click="limpiarPregunta();editarPregunta = false"> <i class="fa fa-plus"></i> Agregar
                                pregunta
                            </a>

                        </div>
                        <div class="center" v-if="!preguntas.length" style="margin-top: 5%">
                            <i>El nivel no tiene preguntas</i>
                        </div>
                        <div v-else class="col s12">
                            <h5 class="center">Preguntas</h5>

                            <div class="card col s12 orange lighten-5" v-for="(pregunta,index) in preguntas">
                                <div class="card-content">

                                    <i><small> #{{index+1}}</small></i>
                                    <div class="title center col s12">
                                        <h5> <b>{{pregunta.CUERPO}}</b>
                                            <div class="secondary-content right">
                                                <a class="waves-effect waves-light btn-small transparent deep-orange-text modal-trigger"
                                                    href="#preguntaForm"
                                                    @click="editarPreguntaMethod(pregunta); indexPreguntaSeleccionada = index">
                                                    <i class="fa fa-pen"></i>
                                                </a>
                                                <button
                                                    class="waves-effect waves-light btn-small transparent deep-orange-text"
                                                    @click="borrarPregunta(pregunta); indexPreguntaSeleccionada = index">
                                                    <i class="fa fa-trash"></i>
                                                </button>



                                            </div>
                                        </h5>

                                    </div>

                                    <div v-if="pregunta.IMG" class="col s12 center">
                                        <img :src="pregunta.IMG" alt="" width="50%">
                                    </div>

                                    <div class="col s12">
                                        <span>Respuestas</span>
                                        <ul class="row collection center">
                                            <li :class="respuesta[1]? 'collection-item green lighten-4' : 'collection-item'"
                                                v-for="respuesta in toJSON(pregunta.RESPUESTAS)">
                                                {{respuesta[0]}}
                                            </li>
                                        </ul>
                                    </div>

                                </div>
                            </div>

                            <div class="right">
                                <a class="modal-trigger btn-small transparent black-text" href="#preguntaForm"
                                    @click="limpiarPregunta"> <i class="fa fa-plus"></i> Agregar pregunta
                                </a>
                            </div>
                            <br><br>

                        </div>

                    </div>
                    <div v-else>
                        <div class="progress">
                            <div class="indeterminate"></div>
                        </div>

                    </div>
                </div>
                <div v-else style="margin-left: 5%">
                    <b><i >Seleccione un nivel...</i></b>
                </div>
            </div>

            <div v-if="pagina == 2">
                Estadísticas
            </div>

        </div>

    </div>


    <div id="nivelForm" class="modal">
        <form v-on:submit.prevent="guardarNivel">
            <div class="modal-content">
                <div class="center">
                    <h4 v-if="!editarNivel">Agregar nivel</h4>
                    <h4 v-else>Editar nivel</h4>
                </div>
                <span><small>Nombre del nivel</small></span>
                <input v-model="nivelCopia.NOMBRE" required />


            </div>
            <div class="modal-footer">
                <button type="" v-if="!editarNivel"
                    class="modal-close btn-flat transparent deep-orange-text">Crear</button>
                <button v-if="editarNivel" class="modal-close btn-flat transparent deep-orange-text">Guardar
                    cambios</button>
                <a class="modal-close waves-effect waves-green btn-flat">Cancelar</a>

            </div>
        </form>
    </div>

    <div id="preguntaForm" class="modal">

        <form v-on:submit.prevent="agregarPregunta">
            <div class="modal-content">
                <h5 v-if="!editarPregunta">Agregar pregunta <span
                        class="transparent transparent-text">{{actualizar}}</span></h5>
                <h5 v-else>Editar pregunta <span class="transparent transparent-text">{{actualizar}}</span></h5>

                <div class="row">
                    <span class="col s12">Pregunta</span>
                    <div class="input-field col s10 m11">
                        <input id="first_name1" placeholder="Escribe aquí la pregunta" type="text" required
                            v-model="pregunta.CUERPO">

                    </div>

                    <button class="col m1 btn transparent black-text" @click="agregarImagen = !agregarImagen"
                        type="button">
                        <i class="fas fa-image"></i>
                    </button>
                </div>
                <div class="input-field col s12" v-if="agregarImagen">
                    <input id="first_name3" type="text" v-model="pregunta.IMG">
                    <label for="first_name3">Link de la imagen (Opcional)</label>
                </div>

                <h6>Respuestas <span class="right">¿Es correcta?</span></h6>
                <div v-for="(res,index) in respuestas" class="row">

                    <div class="input-field col s9 m11">
                        <input id="first_name2" type="text" v-model="res[0]" :placeholder="`Respuesta ${index+1}`">
                    </div>
                    <button
                        :class="res[1]? 'col s3 m1 btn-small transparent green-text': 'col s3 m1 btn transparent red-text'"
                        type="button" @click="res[1] = negar(res[1])">
                        <i :class="res[1]? 'fa fa-check':'fa fa-times'"></i>
                    </button>
                    <button v-if="respuestas.length > 1" class="col s3 m1 btn-small transparent yellow-text"
                        type="button" @click="quitarRespuesta(index)">
                        <i class="fas fa-minus"></i>
                    </button>

                </div>

                <button type="button" class="btn-small green-text transparent" @click="agregarOpcion"><i
                        class="fa fa-plus"></i></button>
                <div class="center col s12">
                    <button type="submit" class="btn transparent green-text modal-close">Guardar Pregunta</button>
                </div>
            </div>

        </form>
    </div>


</div>

<script>
    document.addEventListener('DOMContentLoaded', function () {
        var elems = document.querySelectorAll('.modal');
        var instances = M.Modal.init(elems, {});
    });

    document.addEventListener('DOMContentLoaded', function () {
        var elems = document.querySelectorAll('.dropdown-trigger');
        var instances = M.Dropdown.init(elems, {});
    });

    document.addEventListener('DOMContentLoaded', function () {
        var elems = document.querySelectorAll('.collapsible');
        var instances = M.Collapsible.init(elems, {});
    });

    let app = new Vue({
        el: "#app",
        data: {
            current_user_id: "<?=$this->session->userdata('ID')?>",
            juego_id: "<?=$juego_id?>",
            pagina: 1,
            cargandoHome: false,
            cargandoPreguntas: false,
            juego: {
                "ID": "",
                "CREADOR": "",
                "IMG": "",
                "NOMBRE": "",
                "TIPO": "",
                "CLASIFICACION_ID": "",
                "CLASIFICACION": "",
                "NIVELES": []
            },
            nivel: {
                ID: '',
                NOMBRE: '',
                JUEGO_ID: "<?=$juego_id?>"
            },
            editarNivel: false,
            preguntas: [],
            pregunta: {
                CUERPO: '',
                IMG: '',
                NIVEL_ID: '',
                RESPUESTAS: []
            },
            agregarImagen: false,
            respuestas: [
                ['', true],
                ['', false],
                ['', false]
            ],
            actualizar: false,
            editarPregunta: false['', false],
            indexPreguntaSeleccionada: 0,
            nivelCopia: {
                ID: '',
                NOMBRE: '',
                JUEGO_ID: "<?=$juego_id?>"
            }
        },
        created: function () {
            this.cargarJuego();

        },
        methods: {

            cargarJuego() {
                this.cargandoHome = true;
                axios.get("index.php/api/juego/" + this.juego_id).then(
                    juego => {
                        this.juego = juego.data;
                        this.cargandoHome = false;
                    }
                )
            },
            guardarNivel() {
                if (!this.editarNivel) {
                    axios.post('index.php/api/table/niveles', {
                        data: {
                            nombre: this.nivelCopia.NOMBRE,
                            JUEGO_ID: this.juego_id
                        }
                    }).then(
                        //Volver a cargar los niveles
                        response => {
                            this.cargarJuego();

                        }
                    )
                } else this.guardarCambiosNivel();
            },
            guardarCambiosNivel() {
                let id = this.nivel.ID;
                delete this.nivel.ID;
                axios.put('index.php/api/table/niveles/' + id, {
                    data: {
                        nombre: this.nivelCopia.NOMBRE
                    }
                }).then(
                    //Volver a cargar los niveles
                    reponse => {
                        this.cargarJuego();
                        this.editandoNivel = false;
                        this.cargarJuego();
                        this.nivel.NOMBRE = reponse.data.NOMBRE;
                    }
                )
            },
            creacionNivel() {
                this.nivel.NOMBRE = '';

            },
            edicionNivel() {
                this.nivelCopia = Object.assign({}, this.nivel);
                this.editandoNivel = true;
            },
            cargarPreguntasNivel() {
                this.cargandoPreguntas = true;
                axios.get('index.php/api/preguntasNivel/' + this.nivel.ID).then(
                    response => {
                        this.preguntas = response.data;
                        this.cargandoPreguntas = false;

                    }
                ).catch(
                    error => {
                        this.cargandoPreguntas = false;
                    }
                )
            },
            seleccionarNivel(nivel) {

                this.nivel = Object.assign({}, nivel);
                this.pagina = 1;
                this.cargarPreguntasNivel();
            },
            formatearJson(cadena) {
                console.log('formatearJson');
                console.log(cadena);
                let resultado = "";
                for (letra of cadena) {
                    if (letra == '"') {
                        resultado += "'"
                    } else {
                        resultado += letra;
                    }
                }
                return resultado;
            },

            formatearString(cadena) {
                console.log('Formatear string');
                console.log(cadena);
                let resultado = "";
                for (letra of cadena) {
                    if (letra == "'") {
                        resultado += '"'
                    } else {
                        resultado += letra;
                    }
                }
                return resultado;
            },
            agregarPregunta() {
                if (this.editarPregunta) {
                    this.actualizarPregunta();
                } else {
                    this.pregunta.NIVEL_ID = this.nivel.ID;
                    this.pregunta.RESPUESTAS = this.formatearJson(JSON.stringify(this.respuestas));
                    console.log(this.pregunta.RESPUESTAS);


                    axios.post('index.php/api/table/preguntas', {
                        data: this.pregunta
                    }).then(
                        response => {
                            //this.cargarPreguntasNivel();
                            this.preguntas.push(response.data);
                            this.crearRegistroPreguntaResultados(response.data.ID);
                        }
                    )
                }
            },
            crearRegistroPreguntaResultados(pregunta_id) {
                axios.post('index.php/api/table/preguntas_resultados', {
                    data: {
                        pregunta_id: pregunta_id
                    }
                });
            },
            actualizarPregunta() {
                let id = this.pregunta.ID;
                delete this.pregunta.ID;
                this.pregunta.RESPUESTAS = this.formatearJson(JSON.stringify(this.respuestas));
                axios.put('index.php/api/table/preguntas/' + id, {
                    data: this.pregunta
                }).then(
                    response => {
                        this.limpiarPregunta();
                        this.preguntas[this.indexPreguntaSeleccionada] = response.data;
                    }
                )

                this.editarPregunta = false;
            },
            reiniciarPregunta() {
                this.pregunta = {
                    CUERPO: '',
                    IMG: '',
                    NIVEL_ID: '',
                    RESPUESTAS: []
                }
            },
            borrarPregunta(pregunta) {
                if (confirm("¿Estás seguro de eliminar la pregunta?")) {
                    axios.delete('index.php/api/table/preguntas/' + pregunta.ID).then(
                        response => {
                            this.preguntas.splice(this.indexPreguntaSeleccionada, 1);
                        }
                    )
                }
            },
            agregarOpcion() {
                this.respuestas.push(['', false])
            },
            toJSON(cadena) {
                console.log(cadena);
                objeto = JSON.parse(this.formatearString(cadena));
                return objeto;
            },
            negar(valor) {

                this.actualizar = !this.actualizar;
                return !valor;

            },
            editarPreguntaMethod(pregunta) {
                this.pregunta = Object.assign({}, pregunta);
                this.editarPregunta = true;
                this.respuestas = JSON.parse(this.formatearString(this.pregunta.RESPUESTAS));

            },
            quitarRespuesta(item) {
                this.respuestas.splice(item, 1);
            },
            limpiarPregunta() {
                this.editarPregunta = false;
                this.pregunta.CUERPO = '';
                this.pregunta.IMG = '';
                this.pregunta.RESPUESTAS = '';
                this.respuestas = [
                    ['', true],
                    ['', false],
                    ['', false]
                ];
            },
            eliminarNivel(){
                if(confirm('¿Estás seguro de eliminar el nivel?')){
                    axios.delete('index.php/api/table/niveles/'+this.nivel.ID).then(
                        response => {
                            this.cargarJuego();
                        }
                    ).cath( erros => {
                        alert('Ocurrió un error al eliminar el nivel')
                    })
                }
            }
        },
        computed: {
            toObjeto(cadena) {

                return JSON.parse(cadena);
            }
        }
    });
</script>