<div class="container" id="app">
    <div class="row">
        <div v-if="page === '1'">
            <h4>Juegos</h4>
            <div class="card col m5 s12" @click="page = '2'">
                <div class="card-content">
                    <h2 class="card-title"><b>Quiz Mode</b></h2>
                </div>
            </div>
            <div class="col m1 s12">

            </div>
            <div class="card col m5 s12" @click="page = '3'">
                <div class="card-content">
                    <h2 class="card-title">Clasicos</h2>
                </div>
            </div>
        </div>

        <div v-show="page === '2'" class="col s12">
            <div class="col s12">
               <!--
                    <button v-if="!verJuegos" class="deep-orange-text transparent btn" @click="page = '1'"><i
                        class="fa fa-arrow-left"></i> Regresar a juegos</button>
               -->
                <button v-if="verJuegos" class="deep-orange-text transparent btn" @click="verJuegos = false" ><i
                        class="fa fa-arrow-left"></i> Clasificaciones</button>
            </div>
            <div :class="!clasificacionActual.NOMBRE? 'card col m12 s12' : 'card col m12 s12'" v-if="!verJuegos">
                <div class="card-content">
                    <h2 class="card-title center">
                        <b> Quiz Mode</b>
                    </h2>
                    <a class="right btn deep-orange-text transparent modal-trigger" href="#mensajeModal"
                        @click="limpiarClasificacion()">
                        <i class="fa fa-plus"></i>
                    </a>
                    <table>
                        <thead>
                            <th>Clasificaciones</th>
                            <th style="width: 30%"></th>
                        </thead>
                        <tbody>
                            <tr v-for="clasificacion in clasificaciones"
                                >
                                <td>{{clasificacion.NOMBRE}}</td>
                                <td>
                                    <a class="btn transparent black-text  btn-small"
                                        @click="verClasificacion(clasificacion); verJuegos = true"> <i
                                            class="fa fa-eye"></i> </a>

                                    <button class="btn transparent black-text"
                                        @click="eliminarClasificacion(clasificacion)">
                                        <i class="fa fa-trash"></i>
                                    </button>
                                    <a class="btn transparent black-text modal-trigger" href="#mensajeModal"
                                        @click="iniciarEdicion(clasificacion)">
                                        <i class="fa fa-pen"></i>
                                    </a>


                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="card col s12 m12" v-else>
                <div class="card-content">
                    <div class="col s12">
                        <div class="row">
                            <img :src="clasificacionActual.IMG" class="col s4 center" width="15%">
                            <div class="cart-title col s8">
                                <h5>{{clasificacionActual.NOMBRE}}</h5>
                                <p>
                                    {{clasificacionActual.DESCRIPCION}}
                                </p>
                            </div>
                        </div>
                    </div>

                    <h6 class="center"><b>Juegos</b></h6>
                    <div v-if="cargandoJuegos">
                        <div class="progress">
                            <div class="indeterminate"></div>
                        </div>
                    </div>
                    <div v-else>
                        <a class="right btn deep-orange-text transparent modal-trigger" href="#juegoForm"
                            @click="limpiarJuego()">
                            <i class="fa fa-plus"></i>
                        </a>

                        <ul class="collection col s12" v-if="clasificacionActual.JUEGOS.length">
                            <li class="collection-item avatar" v-for="juego in clasificacionActual.JUEGOS">
                                <img :src="juego.IMG" alt="" class="circle">
                                <span class="title">{{juego.NOMBRE}}</span>
                                <p>
                                    <small><i>Creado por: </i>{{juego.CREADOR}}</small>

                                </p>
                                <div class="secondary-content">
                                    <a class="btn transparent deep-orange-text btn-small"
                                        :href="`index.php/inicio/juego/${juego.ID}`"><i class="fa fa-eye"></i></a>
                                    <button class="btn transparent deep-orange-text btn-small"><i class="fa fa-trash"
                                            @click="eliminarJuego(juego)"></i></button>
                                    <a class="btn transparent deep-orange-text btn-small modal-trigger"
                                        href="#juegoForm" @click="iniciarEdicionJuego(juego)">
                                        <i class="fa fa-pen"></i>
                                    </a>
                                </div>
                            </li>

                        </ul>

                        <div v-else class="center col s12">
                            <i>No se encontraron juegos</i>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
    <div id="mensajeModal" class="modal">
        <form v-on:submit.prevent="guardarClasificacion">
            <div class="modal-content">
                <div class="center">
                    <h4 v-if="!editar">Nueva clasificación</h4>
                    <h4 v-if="editar">Editar clasificación</h4>
                </div>
                <span><small>Nombre de la clasificación</small></span>
                <input v-model="clasificacion.NOMBRE" required />
                <span><small>Descripción</small></span>
                <input v-model="clasificacion.DESCRIPCION" required />
                <span><small>Dirección de la imagen</small></span>
                <input v-model="clasificacion.IMG" />
            </div>
            <div class="modal-footer">
                <button type="" v-if="!editar" class="modal-close btn-flat transparent deep-orange-text">Crear</button>
                <button v-if="editar" class="modal-close btn-flat transparent deep-orange-text">Guardar cambios</button>
                <a class="modal-close waves-effect waves-green btn-flat">Cancelar</a>

            </div>
        </form>
    </div>

    <div id="juegoForm" class="modal">
        <form v-on:submit.prevent="guardarJuego">
            <div class="modal-content">
                <div class="center">
                    <h4 v-if="!editarJuego">Crear juego</h4>
                    <h4 v-if="editarJuego">Editar juego</h4>
                </div>
                <span><small>Nombre del juego</small></span>
                <input v-model="juego.NOMBRE" required />

                <span><small>Dirección de la imagen</small></span>
                <input v-model="juego.IMG" />
            </div>
            <div class="modal-footer">
                <button type="" v-if="!editarJuego"
                    class="modal-close btn-flat transparent deep-orange-text">Crear</button>
                <button v-if="editarJuego" class="modal-close btn-flat transparent deep-orange-text">Guardar
                    cambios</button>
                <a class="modal-close waves-effect waves-green btn-flat">Cancelar</a>

            </div>
        </form>
    </div>



</div>

<script>
    document.addEventListener('DOMContentLoaded', function () {
        var elems = document.querySelectorAll('.modal');
        var instances = M.Modal.init(elems, {});
    });

    document.addEventListener('DOMContentLoaded', function () {
        var elems = document.querySelectorAll('.collapsible');
        var instances = M.Collapsible.init(elems, {});
    });


    let app = new Vue({
        el: "#app",
        data: {
            current_user_id: "<?=$this->session->userdata('ID')?>",
            page: '2',
            clasificaciones: [],
            clasificacion: {
                NOMBRE: '',
                DESCRIPCION: '',
                IMG: ''
            },
            editar: false,
            clasificacionActual: {
                NOMBRE: '',
                JUEGOS: []
            },
            juego: {
                "ID": "",
                "CREADOR_ID": this.current_user_id,
                "IMG": "",
                "NOMBRE": "",
                "CLASIFICACION_ID": ""
            },
            editarJuego: false,
            cargandoJuegos: false,
            verJuegos: false
        },
        created: function () {
            this.cargarClasificaciones();
        },
        methods: {
            cargarClasificaciones() {
                axios.get('index.php/api/table/clasificaciones').then(
                    result => {
                        this.clasificaciones = result.data;
                    }
                ).catch(
                    error => {
                        alert('Ocurrio in error')
                    }
                );
            },
            crearClasificacion() {
                axios.post('index.php/api/table/clasificaciones', {
                    data: this.clasificacion
                }).then(
                    ok => {
                        this.cargarClasificaciones();

                    }
                )
            },
            eliminarClasificacion(clasificacion) {
                if (confirm(`¿Estás seguro de eliminar la clasificación '${clasificacion.NOMBRE}'?`)) {
                    axios.delete('index.php/api/table/clasificaciones/' + clasificacion.ID).then(
                        ok => {
                            this.cargarClasificaciones();
                            this.clasificacionActual.NOMBRE = ''
                        }
                    )
                }
            },
            iniciarEdicion(clasificacion) {
                this.editar = true;
                this.clasificacion = Object.assign({}, clasificacion);
            },
            guardarClasificacion() {
                if (this.editar) {
                    let id = this.clasificacion.ID;
                    delete this.clasificacion.ID;
                    axios.put('index.php/api/table/clasificaciones/' + id, {
                        data: this.clasificacion
                    }).then(
                        ok => {

                            this.cargarClasificaciones();
                            let clasificacionR = ok.data;
                            this.clasificacionActual.NOMBRE = clasificacionR.NOMBRE;
                            this.clasificacionActual.DESCRIPCION = clasificacionR.DESCRIPCION;
                            this.clasificacionActual.IMG = clasificacionR.IMG;
                        }
                    )
                } else {
                    this.crearClasificacion();
                }
            },
            verClasificacion(clasificacion) {
                this.clasificacionActual = Object.assign({}, clasificacion);
                this.cargarJuegos();
            },
            cargarJuegos() {
                this.cargandoJuegos = true;
                axios.get('index.php/api/juegos/' + this.clasificacionActual.ID).then(
                    ok => {
                        this.clasificacionActual.JUEGOS = ok.data;
                        this.cargandoJuegos = false;
                    }
                ).catch(
                    error => {
                        this.clasificacionActual.JUEGOS = [];
                        this.cargandoJuegos = false;
                    }
                )
            },
            limpiarClasificacion() {
                this.editar = false;
                this.clasificacion.NOMBRE = '';
                this.clasificacion.DESCRIPCION = '';
                this.clasificacion.IMG = '';
            },
            limpiarJuego() {
                this.juego = {
                    "ID": "",
                    "CREADOR_ID": this.current_user_id,
                    "IMG": "",
                    "NOMBRE": "",
                    "CLASIFICACION_ID": ""
                }
            },
            guardarJuego() {
                if (this.editarJuego) {
                    this.guardarDatosJuego();
                } else {
                    this.crearJuego();
                }
            },
            guardarDatosJuego() {
                let id = this.juego.ID;
                delete this.juego.ID;
                delete this.juego.CLASIFICACION_ID;
                delete this.juego.CREADOR;

                axios.put('index.php/api/table/juegos/' + id, {
                    data: this.juego
                }, {
                    data: this.juego
                }).then(
                    ok => {
                        this.cargarJuegos();
                    }
                )
            },
            crearJuego() {
                delete this.juego.ID;
                this.juego.CLASIFICACION_ID = this.clasificacionActual.ID;
                axios.post('index.php/api/table/juegos', {
                    data: this.juego
                }).then(
                    ok => {
                        this.cargarJuegos();
                    }
                )
            },
            eliminarJuego(juego) {
                {}
                if (confirm(`¿Estás seguro de eliminar el juego ${juego.NOMBRE}?`)) {
                    axios.delete('index.php/api/table/juegos/' + juego.ID).then(ok => {
                        this.cargarJuegos()
                    });
                }
            },
            iniciarEdicionJuego(juego) {
                this.editarJuego = true;
                this.juego = Object.assign({}, juego);
            }
        }
    });
</script>