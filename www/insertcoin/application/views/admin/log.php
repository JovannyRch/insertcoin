<div class="container" id="app">
    <div class="row animated bounceIn" v-show="!cargando">
            <div class="text-center center">
                    <h3>Logs del sistema</h3>
                </div>

        <div class="col s4 left-align">
            Pagina {{pagina}} de {{calculo}}
            <br>
            <span class="col s5">Ir a la pagina</span>
            <div class="col s7">
                <input type="number" v-model="pagina" @change="getData">
            </div>
        </div>
        <div class="center-align col s12">
                <button class="btn green" @click="anteriorPagina">
                        <i class="fas fa-angle-left"></i> Anterior pagina
                </button>
                <button class="btn green" @click="siguientePagina">
                        Siguiente pagina <i class="fas fa-angle-right"></i> 
                </button>

            <br>
            <br>

        </div>
        <table>
            <thead>
                <th>#</th>
                <th>Accion</th>
                <th>Fecha</th>
            </thead>
            <tbody>
                <tr v-for="l in logs">
                    <td>{{l.RN}}</td>
                    <td>{{l.ACCION}}</td>
                    <td>{{l.FECHA}}</td>
                </tr>
            </tbody>
        </table>

    </div>
    <div class="progress" v-show="cargando">
        <div class="indeterminate"></div>
    </div>

</div>

<script>
    document.addEventListener('DOMContentLoaded', function () {
        var elems = document.querySelectorAll('.modal');
        var instances = M.Modal.init(elems, {});
    });


    let app = new Vue({
        el: "#app",
        data: {
            current_user_id: "<?=$this->session->userdata('ID')?>",
            cantidad: parseInt("<?=$cantidad?>"),
            cargando: false,
            pagina: 0,
            registros: 200,
            logs: []
        },
        created: function () {
            this.getData();
        },
        methods: {
            getData() {
                this.cargando = true;
                axios.get(`index.php/api/logs/${this.pagina}/${this.registros}`).then(
                    response => {
                        this.logs = response.data;
                        this.cargando = false;
                    }
                ).catch(
                    error => {
                        this.cargando = false;
                        alert("Ocurrio un error");
                    }
                )
            },
            anteriorPagina() {
                if (this.pagina != 0) {
                    this.pagina -= 1;
                    this.getData();

                }
            },
            siguientePagina() {
                this.pagina += 1;
                this.getData();
            }
        },
        computed: {
            calculo() {
                return parseInt(this.cantidad / this.registros);
            }
        }
    });
</script>