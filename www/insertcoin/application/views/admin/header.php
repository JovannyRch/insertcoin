<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">

    <base href="<?php echo $this->config->base_url()?>" />
    <link rel="stylesheet" href="assets/css/materialize.min.css">
    <link rel="shortcut icon" href="assets/img/icon.png" type="image/x-icon">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="assets/js/vue.js"></script>
    <script src="assets/js/axios.min.js"></script>
    <script src="assets/js/highcharts.js"></script>

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <title>InsertCoin</title>

</head>

<body >
<nav class="deep-orange lighten-1" style="  margin-bottom: 3%;">
    <div class="nav-wrapper container ">
      <a href="index.php/inicio" class="brand-logo">InsertCoin.com</a>

      <ul id="nav-mobile" class="right hide-on-med-and-down">
      <li> <a href="index.php/inicio/log"> <i class="fas fa-list-alt"></i> </a> </li>
        <li><a href="index.php/inicio/publicaciones"> <i class="fas fa-newspaper"></i></a></li>
        <li><a href="index.php/inicio/juegos"><i class="fas fa-gamepad"></i></a></li>
        <li><a href="index.php/inicio/usuarios"> <i class="fa fa-users"> </i> </a></li>
        <li><a href="index.php/inicio/conversaciones"><i class="fas fa-envelope"></i></a></li>
        <li><a href="index.php/inicio/paises"> <i class="fas fa-globe-americas"></i></a></li>
        <li> <a href="index.php/usuarios/logout"> <i class="fas fa-sign-out-alt"></i> </a> </li>
        <li><a href="index.php/inicio/perfil"><?=$this->session->userdata('USERNAME')?></a></li>
      </ul>
    </div>
    
  </nav>

  


  