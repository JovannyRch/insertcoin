<div class="container" id="app">
    <div class="row animated fadeIn" v-show="!cargando">

        <div class="right">
            <button class="btn orange" >Nuevo juego</button>
        </div>
        
        <table>
            <thead>
                <th style="width:3%">#</th>
                <th style="width:5%"></th>
                <th>Juego</th>
                <th>Creador</th>
                <th>Clasificacion</th>
            </thead>
            <tbody>
                <tr v-for="(j,index) in juegos">
                    <td>
                        {{index+1}}
                    </td>
                    <td>
                        <img :src="j.IMG"  width="50%">
                    </td>
                    <td>
                        {{j.NOMBRE}}
                    </td>
                    <td>
                        {{j.CREADOR}}
                    </td>
                    <td>
                        {{j.CLASIFICACION}}
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="progress" v-show="cargando">
        <div class="indeterminate"></div>
    </div>

</div>

<script>
    document.addEventListener('DOMContentLoaded', function () {
        var elems = document.querySelectorAll('.modal');
        var instances = M.Modal.init(elems, {});
    });


    let app = new Vue({
        el: "#app",
        data: {
            current_user_id: "<?=$this->session->userdata('ID')?>",
            cargando: true,
            juegos: []
        },
        created: function () {
            this.getJuegos();
        },
        methods: {
            getJuegos() {
                this.cargando = true;
                axios.get("index.php/api/juegosClas").then(response => {
                    this.juegos = response.data;
                    this.cargando = false;

                }).catch(error => {
                    this.cargando = false;
                })
            }
        }
    });
</script>