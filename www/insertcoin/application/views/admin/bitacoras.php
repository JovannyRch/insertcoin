<div class="container" id="app">
    <div class="row animated bounceIn" v-show="!cargando">
        <div class="text-center center">
            <h3>Bitacoras</h3>
        </div>
        <div class="col s6">

            <select v-model="usuarioActual" class=" browser-default">
                <option value="">Seleccione un usuario</option>
                <option value="administrador">Administrador</option>
                <option value="desarrollador">Desarrollador</option>
                <option value="verificador">Verificador</option>
                <option value="publicista">Publicista</option>
            </select>
        </div>

        <div class="col s12">
            <ul class="collapsible">
                <li>
                    <div class="collapsible-header">Operaciones con fecha</div>
                    <div class="collapsible-body">
                        <br>
                            <h5>Consultar registros de una fecha</h5>
                        <div class="col s8">
                            <input type="date" v-model="fecha1">
                        </div>
                        <div class="col s4">
                            <button class="btn deep-orange white-text" @click="cargarDatos">Consultar</button>
                        </div>
                        <br><br>
                        <h5>Consultar registros entre dos fecha</h5>
                        <div class="col s4">
                            Fecha Menor <br>
                            <input type="date" v-model="fecha2">
                        </div>
                        <div class="col s4">
                            Fecha Mayor <br>
                            <input type="date" v-model="fecha3">
                        </div>
                        <div class="col s4">
                                <button class="btn deep-orange white-text" @click="cargarDatos">Consultar</button>
                            </div>
                            <br>
                            <br>
                            <br>
                            <br>
                    </div>
                </li>

            </ul>
        </div>
        <div v-if="usuarioActual" class="col s12">
            <table>
                <thead>
                    <th>#</th>
                    <th>Acción</th>
                    <th>Fecha</th>
                </thead>
                <tbody>
                    <tr v-for="(r,index) in bitacoras[usuarioActual]">
                        <td>{{index+1}}</td>
                        <td>{{r.ACCION}}</td>
                        <td>{{r.FECHA}}</td>
                    </tr>
                </tbody>
            </table>
        </div>

    </div>
    <div class="progress" v-show="cargando">
        <div class="indeterminate"></div>
    </div>

</div>

<script>
    document.addEventListener('DOMContentLoaded', function () {
        var elems = document.querySelectorAll('.modal');
        var instances = M.Modal.init(elems, {});
    });

    document.addEventListener('DOMContentLoaded', function () {
        var elems = document.querySelectorAll('.collapsible');
        var instances = M.Collapsible.init(elems, {});
    });

    let app = new Vue({
        el: "#app",
        data: {
            current_user_id: "<?=$this->session->userdata('ID')?>",
            cargando: false,
            bitacoras: [],
            usuarioActual: '',
            fecha1: '',
            fecha2: '',
            fecha3: ''
        },
        created: function () {
            this.cargarDatos();
        },
        methods: {
            cargarDatos() {
                this.cargando = true;
                params = '';

                if(this.fecha2 && this.fecha3){
                    params = 'fecha2/'+this.fecha2+'/fecha3/'+this.fecha3;
                    this.fecha1 = '';
                }

                if(this.fecha1){
                    params = 'fecha1/'+this.fecha1;
                    this.fecha2 = '';
                    this.fecha3 = '';
                }
                
                axios.get('index.php/api/bitacoras/'+params).then(
                    response => {
                        this.cargando = false;
                        this.bitacoras = response.data;
                    }
                ).catch(
                    error => {
                        this.cargando = false;
                        alert("Ocurrio un error al cargar los datos");
                    }
                )
            }
        }
    });
</script>