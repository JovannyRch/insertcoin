<div class="container" id="app">



    <div class="row">

       

        <div class="col s12 m5">
            <h4>Jugadores <i class="fa fa-users"></i> </h4>

            <table>
                <tbody>
                    <tr v-for="jugador in jugadores">
                        <td style="width: 5%"><img :src="jugador.AVATAR" width="25px"></td>
                        <td><a :href="`index.php/inicio/usuario/${jugador.USERNAME}`">{{jugador.USERNAME}}</a></td>
                        <td>{{jugador.FECHA_REGISTRO}}</td>
                        <td style="width: 5%"><img :src="jugador.IMG" width="25px"></td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="col m1"></div>
        <div class="col s12 m5">
            <h4>Administradores <i class="fa fa-users"></i> </h4>
            <table>
                <tbody>
                    <tr v-for="jugador in administradores">
                        <td style="width: 5%"><img :src="jugador.AVATAR" width="25px"></td>
                        <td><a :href="`index.php/inicio/usuario/${jugador.USERNAME}`">{{jugador.USERNAME}}</a></td>
                        <td>{{jugador.FECHA_REGISTRO}}</td>
                        <td style="width: 5%"><img :src="jugador.IMG" width="25px"></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>


<script>
    document.addEventListener('DOMContentLoaded', function () {
        var elems = document.querySelectorAll('.modal');
        var instances = M.Modal.init(elems, {});
    });

    document.addEventListener('DOMContentLoaded', function () {
        var elems = document.querySelectorAll('.dropdown-trigger');
        var instances = M.Dropdown.init(elems, {});
    });

    document.addEventListener('DOMContentLoaded', function () {
        var elems = document.querySelectorAll('.collapsible');
        var instances = M.Collapsible.init(elems, {});
    });


    let app = new Vue({
        el: "#app",
        data: {
            usuario: "<?=$this->session->userdata('ID')?>",
            jugadores: [],
            administradores: []
        },
        created: function () {
            this.cargarJugadores();
            this.cargarAdministradores();
        },
        methods: {
            cargarJugadores() {
                axios.get('index.php/api/table/jugadores').then(
                    jugadores => {
                        this.jugadores = jugadores.data;
                    },
                    error => console.log("error al cargar los jugadores")

                )
            },
            cargarAdministradores() {
                axios.get('index.php/api/table/administradores').then(
                    administradores => {
                        this.administradores = administradores.data;
                    },
                    error => console.log("error al cargar los administradores")

                )
            }
        }
    });
</script>