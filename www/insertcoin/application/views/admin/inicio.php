<div class="container " id="app">
    <div class="row animated fadeIn " v-show="!cargando">
        <div class="col s12 card">
            <div class="card-content">
                <ul class="collection">
                        <li class="collection-item"><a href="index.php/inicio/bitacoras" class="btn black-text orange lighten-4">Ver bitacora de usuarios de la Base de Datos</a></li>
                        <li class="collection-item"><a href="index.php/inicio/log" class="btn black-text orange lighten-4">Ver bitacora de InsertCoin.com</a></li>
                        <li class="collection-item"><a href="index.php/inicio/historial" class="btn black-text orange lighten-4">Ver historial de partidas</a></li>
                </ul>
            </div>
        </div>

        <div class="col s6 card">
            <div class="card-content">
                <div class="col s8">
                    <h4>
                        <span style="font-size:45px"> <b>{{datos.usuarios}}</b> </span>
                    </h4>
                    Usuarios registrados
                </div>
                <div class="col s4">
                    <i class="fa fa-users fa-4x right deep-orange-text"></i>

                </div>
            </div>
        </div>
        <div class="col s6 card">
            <div class="card-content">
                <div class="col s8">
                    <h4>
                        <span style="font-size:45px"> <b>{{datos.mensajes}}</b> </span>
                    </h4>
                    Mensajes enviados

                </div>
                <div class="col s4">
                    <i class="fa fa-envelope fa-4x right deep-orange-text"></i>

                </div>
            </div>
        </div>

        <div class="col s6 card">
            <div class="card-content">
                <div class="col s8">
                    <h4>
                        <span style="font-size:45px"> <b>{{datos.juegos}}</b> </span>
                    </h4>
                    Juegos

                </div>
                <div class="col s4">
                    <i class="fa fa-gamepad fa-4x right deep-orange-text"></i>

                </div>
            </div>
        </div>



        <div class="col s6 card">
            <div class="card-content">
                <div class="col s8">
                    <h4>
                        <span style="font-size:45px"> <b>{{datos.intentos}}</b> </span>
                    </h4>
                    Partidas jugadas
                    <small><a href="index.php/inicio/historial">Ver Historial</a></small>
                </div>
                <div class="col s4">
                    <i class="fa fa-play fa-4x right deep-orange-text"></i>

                </div>
            </div>
        </div>
        <div class="col s12 card">
            <div class="card-content">
                <div id="grafica1">

                </div>
            </div>
        </div>

        <div class="col s12 card">
            <div class="card-content">
                <div class="col s6">
                    <h5 class="center deep-orange-text">Juegos más jugados</h5>
                    <table>
                        <thead>
                            <th style="width: 3%">#</th>
                            <th style="width: 8%"></th>
                            <th>Juego</th>
                            <th>Veces jugado</th>
                        </thead>
                        <tbody>
                            <tr v-for="(juego,index) in datos.topJuegos">
                                <td>{{index+1}}</td>
                                <td>
                                    <img :src="juego.IMG" width="100%">

                                </td>
                                <td>{{juego.NOMBRE}}</td>
                                <td>{{juego.VECES_JUGADO}}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col s6">
                    <h5 class="center deep-orange-text">Jugadores más activos</h5>
                    <table>
                        <thead>
                            <th style="width: 3%">#</th>
                            <th style="width: 8%"></th>
                            <th>Jugador</th>
                            <th>Veces jugado</th>
                        </thead>
                        <tbody>
                            <tr v-for="(jugador,index) in datos.topActivos">
                                <td>{{index+1}}</td>
                                <td>
                                    <img :src="jugador.AVATAR" width="100%">
                                </td>
                                <td> <a :href="`index.php/inicio/usuario/${jugador.USERNAME}`"> {{jugador.USERNAME}}
                                    </a></td>
                                <td>{{jugador.TOTAL}}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="col s12 card">
            <div class="card-content">
                <div id="grafica2">

                </div>
            </div>
        </div>

        <div class="col s12 card">
            <div class="card-content">
                <div id="grafica3">

                </div>
            </div>
        </div>

    </div>
    <div class="progress" v-show="cargando">
        <div class="indeterminate"></div>
    </div>

</div>

<script>
    let app = new Vue({
        el: "#app",
        data: {
            current_user_id: "<?=$this->session->userdata('ID')?>",
            cargando: false,
            datos: {}
        },
        created: function () {
            this.getDatos()
        },
        methods: {
            getDatos() {
                this.cargando = true;
                axios.get("index.php/api/reporte").then(
                    response => {
                        this.datos = response.data;
                        this.graficas();
                        this.cargando = false;
                    }
                ).catch(
                    error => {
                        this.cargando = false;
                    }
                )
            },
            graficas() {
                Highcharts.chart('grafica1', {

                    title: {
                        text: 'Veces jugado en los últimos 15 días'
                    },
                    xAxis: {
                        categories: app.datos.ultimosdias.dias
                    },
                    yAxis: {
                        title: {
                            text: 'Cantidad'
                        }
                    },
                    legend: {
                        layout: 'vertical',
                        align: 'right',
                        verticalAlign: 'middle'
                    },


                    series: [{
                        name: 'Veces',
                        data: app.datos.ultimosdias.cantidades
                    }]

                });

                Highcharts.chart('grafica2', {
                    chart: {
                        type: 'column'
                    },
                    title: {
                        text: 'Cantidad de jugadores por país'
                    },
                    xAxis: {
                        categories: app.datos.paises.paises
                    },
                    yAxis: {
                        title: {
                            text: 'Cantidad'
                        }
                    },
                    legend: {
                        layout: 'vertical',
                        align: 'right',
                        verticalAlign: 'middle'
                    },

                    series: [{
                        name: 'Cantidad de jugadores',
                        data: app.datos.paises.totales
                    }]

                });

                Highcharts.chart('grafica3', {
                    chart: {
                        type: 'column'
                    },
                    title: {
                        text: 'Cantidad de juegos por clasificacion'
                    },
                    xAxis: {
                        categories: app.datos.clasificaciones.nombres
                    },
                    yAxis: {
                        title: {
                            text: 'Cantidad'
                        }
                    },
                    legend: {
                        layout: 'vertical',
                        align: 'right',
                        verticalAlign: 'middle'
                    },

                    series: [{
                        name: 'Cantidad de juegos',
                        data: app.datos.clasificaciones.totales
                    }]

                });
            }
        }
    });
</script>