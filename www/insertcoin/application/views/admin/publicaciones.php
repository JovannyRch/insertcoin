<div class="container" id="app">
    <div class="row">
        <h2>Publicaciones</h2>
        <div class="col s12 right-align">
            <button class="btn transparent orange-text modal-trigger" href="#modal1" @click="resetPost">Crear publicación</button>
        </div>
        <ul class="collection">
            <li class="collection-item avatar" v-for="publicacion in publicaciones" >
                <img v-if="publicacion.IMG" :src="publicacion.IMG" alt="" class="circle">
                <span class="title"><b>{{publicacion.TITULO}}</b></span>
                <br>
                {{publicacion.FECHA}}
                </p>
                <div class="secondary-content">

                    
                    <a class="btn btn-small transparent green-text" :href="`index.php/inicio/publicacion/${publicacion.ID}`">
                        ver
                    </a>

                    <button class="btn btn-small transparent orange-text modal-trigger" href="#modal1" @click="clonarPost(publicacion)"> <i
                            class="fa fa-edit"></i> </button>
                    <button class="btn btn-small transparent red-text" @click="borrarPost(publicacion)"> <i
                            class="fa fa-trash"></i> </button>
                </div>

            </li>

        </ul>

        <!-- Modal Structure -->
        <div id="modal1" class="modal modal-fixed-footer">
            <div class="modal-content">
                 <h4 v-if="!editar">Crear publicación</h4>
                 <h4 v-else>Editar publicación</h4>

                <span>Título</span>
                <input type="text" v-model="publicacion.TITULO" placeholder="Escriba aquí el título">



                <span>Link de la imagen</span>
                <input type="text" v-model="publicacion.IMG" placeholder="Escriba aquí la dirección de la imagen">

                <span for="textarea1">Contenido</span>
                <textarea id="textarea1" class="materialize-textarea" v-model="publicacion.CUERPO"
                    placeholder="Escriba aquí el contenido"></textarea>

            </div>
            <div class="modal-footer">
                <a class="modal-close waves-effect waves-green btn-flat green-text" @click="crearPost" v-if="!editar">Crear</a>
                <a class="modal-close waves-effect waves-green btn-flat green-text" @click="actualizarPost" v-else >Guardar cambios</a>
                <a class="modal-close waves-effect waves-green btn-flat">Cancelar</a>
            </div>
        </div>



    </div>

</div>

<script>
    document.addEventListener('DOMContentLoaded', function () {
        var elems = document.querySelectorAll('.modal');
        var instances = M.Modal.init(elems, {});
    });


    let app = new Vue({
        el: "#app",
        created: function () {
            this.cargarPublicaciones();
        },
        data: {
            publicaciones: [],
            publicacion: {
                TITULO: '',
                CUERPO: '',
                IMG: ''
            },
            editar: false
        },
        methods: {
            cargarPublicaciones() {
                axios.get('index.php/api/table/postadmin').then(
                    response => this.publicaciones = response.data,
                    error => console.log('Error al cargar las publicaciones')
                )
            },
            crearPost() {
                
                axios.post('index.php/api/publicacion', {
                    publicacion: this.publicacion
                }).then(
                    response => {
                        this.cargarPublicaciones();
                    },
                    error => console.log('Error al crear la publicación')
                )
            },
            actualizarPost() {
                const id = this.publicacion.ID;
                delete this.publicacion.ID;
                axios.put('index.php/api/table/posts_admin/'+id, {data: this.publicacion}).then(
                    result => this.cargarPublicaciones(),
                    error => console.log('Error al actualizar al publicacion')
                )
            },
            borrarPost(publicacion) {
                if (confirm(`¿Estás seguro de eliminar la publicación '${publicacion.TITULO}'?`)) {
                    axios.delete('index.php/api/table/posts_admin/' + publicacion.ID).then(
                        response => this.cargarPublicaciones(),
                        error => console.log('Error al eliminar')

                    )
                }
            },
            clonarPost(publicacion) {
                this.publicacion = Object.assign({}, publicacion);
                this.editar = true;
            },
            resetPost() {
                this.editar = false;
                this.publicacion = {
                    TITULO: '',
                    CUERPO: '',
                    IMG: ''
                }
            },
        }
    });
</script>