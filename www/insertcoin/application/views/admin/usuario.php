<div class="container" id="app">
    <div class="row">
        <div class="col s12 m12 ">
            <div class="card horizontal transparent">
                <div class="card-image">
                    <img v-if="usuario.AVATAR" :src="usuario.AVATAR" style="padding: 10%; max-height: 15rem">
                    <img v-else src="assets/img/avatars/1.png" style="padding: 10%; max-height: 15rem">
                </div>
                <div class="card-stacked">
                    <div class="card-content">
                        <h4>{{usuario.USERNAME}}  <img width="45px" :src="usuario.pais.IMG"> </h4> 
                        <i>Miembre desde {{usuario.FECHA_REGISTRO}}</i>
                        <p>
                            {{usuario.BIO}}
                        </p>
                    </div>
                    <div class="card-action">
                        <a  class="modal-trigger right" href="#mensajeModal"> <i class="fa fa-envelope"></i> </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col s9 ">
                <br>
                <br>
            <h6 class=""><b>Notas</b></h6>


            <textarea  class="materialize-textarea" placeholder="Escribe una nota..." v-model="nota.CONTENIDO">
                </textarea>
            <button class="btn deep-orange lighten-2" @click="publicarNota"
                v-if="nota.CONTENIDO">Publicar</button>
               
            <div v-if="usuario.notas.length" >
                <ul class="collection">
                    <li class="collection-item avatar" v-for="(nota,index) in usuario.notas">
                        <img :src="nota.AVATAR" alt="" class="circle">
                        <span class="title"> <a
                                :href="`index.php/inicio/usuario/${nota.USERNAME}`">{{nota.USERNAME}}
                            </a></span> <small class="grey-text lighten-2">{{nota.FECHA}}</small>
                        <p> {{nota.NOTA}}
                        </p>
                        <a @click="eliminarNota(nota.ID, index)" v-if="current_user === usuario.ID || nota.AUTOR_ID == current_user"
                            class="secondary-content btn-floating-text btn-small transparent"> <i
                                class=" black-text  fa fa-trash" v-if=""></i> </a>
                    </li>
                </ul>
            </div>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>

        </div>


        <div class="col s3">
        <br>
                <br>
            <h6 class="center deep-orange-text"><b>Records Personales</b></h6>
            <table>
                <thead>
                    <th style="width:3%">#</th>
                    <th style="width:20%"></th>
                    <th>Juego</th>
                    <th>Record</th>
                </thead>
                <tbody>
                    <tr v-for="(r,index) in records">
                        <td>{{index+1}}</td>
                        <td>
                            <img :src="r.IMG" width="100%">
                        </td>
                        <td><a :href="`index.php/inicio/juego/${r.ID}/stats/${usuario.ID}`"> {{r.NOMBRE}} </a></td>
                        <td>{{r.RECORD}}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>


    <div id="mensajeModal" class="modal">
            <div class="modal-content">
                <h4 >Enviar mensaje a <i> {{usuario.USERNAME}} </i></h4>
                <textarea name="" id="" cols="30" rows="50" v-model="mensaje"></textarea>
    
            </div>
            <div class="modal-footer">
                <a @click="enviarMensaje" class="modal-close waves-effect waves-green btn-flat green-text">Enviar mensaje</a>
                <a class="modal-close waves-effect waves-green btn-flat">Cancelar</a>
            </div>
        </div>

</div>

<script>
    document.addEventListener('DOMContentLoaded', function () {
        var elems = document.querySelectorAll('.modal');
        var instances = M.Modal.init(elems, {});
    });


    let app = new Vue({
        el: "#app",
        data: {
            current_user: "<?=$this->session->userdata('ID')?>",
            username: "<?=$usuario?>",
            usuario: {
                AVATAR: '',
                BIO: '',
                ESTADO_ID: "",
                FECHA_NACIMIENTO: '',
                FECHA_REGISTRO: "",
                ID: "",
                PAIS_ID: '',
                USERNAME: "<?=$usuario?>",
                "pais": {
                    "ID": "",
                    "NOMBRE": "",
                    "IMG": ""
                },
                notas: []
            },
            nota: {
                CONTENIDO: ''
            },
            mensaje: '',
            records: []
        },
        created: function () {
            this.cargarUsuario();
            
        },
        methods: {
            cargarRecords(){
                axios.get("index.php/api/recordsJugador/"+this.usuario.ID).then(
                    response => {
                        this.records = response.data;
                    }
                )
            },
            cargarUsuario() {
                axios.get('index.php/api/usuario/' + this.username).then(
                    usuario => {
                        this.usuario = usuario.data;
                        this.cargarRecords();
                    },
                    error => console.log('error al cargar la informacion del usuario')
                )
            },
            publicarNota(){
                let data= {
                    usuario_id: this.usuario.ID,
                    autor_id: "<?=$this->session->userdata('ID')?>",
                    nota: this.nota.CONTENIDO
                };

                axios.post('index.php/api/table/notas_usuario',{data: data}).then(
                    reponse => {
                        this.cargarUsuario();
                        this.nota.CONTENIDO = "";
                    },
                    error => console.log('Error al publicar la nota')
                )
            },
            eliminarNota(nota_id,index){
                if(confirm("¿Estás seguro de eliminar la nota?")){
                    axios.delete('index.php/api/table/notas_usuario/'+nota_id).then(
                        result=> {
                            this.usuario.notas.splice(index,1);
                        },
                        error => {
                            console.log('Ocurrio un error al eliminar la nota');
                        }
                    )
                }
            },
            enviarMensaje(){
                if(this.mensaje){
                    data = {
                        mensaje: this.mensaje,
                        emisor: this.current_user,
                        receptor: this.usuario.ID
                    }
                    axios.post('index.php/api/enviarMensaje',data).then(
                        ok => {
                            alert('Mensaje enviado');
                            this.mensaje = '';
                        },
                        error => alert('Ocurrió un error al enviar el mensaje')
                    )
                }
            }
        }
    });
</script>