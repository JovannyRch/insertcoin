<div class="container" id="app">
    <div class="row">
        <button @click="limpiarPais" class="btn-floating btn-small waves-effect waves-light transparent modal-trigger"
            href="#paisForm"><i class="fa fa-plus black-text"></i></button>

        <table class="striped col s12 m8">
            <thead>
                <th>Bandera</th>
                <th>Nombre</th>
                <th></th>
            </thead>
            <tbody>
                <tr v-for="pais in paises">
                    <td style="width: 10%"><img :src="pais.IMG" width="25px"></td>
                    <td>{{pais.NOMBRE}}</td>
                    <td style="width: 13%">

                        <button @click="borrarPais(pais)"
                            class="btn-floating btn-small waves-effect waves-light transparent"><i
                                class="fa fa-trash black-text"></i></button>
                        <button @click="editarPais(pais)"
                            class="btn-floating btn-small waves-effect waves-light transparent modal-trigger"
                            href="#paisForm"><i class="fa fa-pen black-text"></i></button>

                    </td>
                </tr>
            </tbody>
        </table>

    </div>

    <div id="paisForm" class="modal">
        <div class="modal-content">
            <h4 v-if="!editar">Agregar País</h4>
            <h4 v-else>Editar País</h4>
            <span>Nombre</span>

            <input type="text" v-model="pais.NOMBRE">

            <div v-show="pais.NOMBRE && !pais.IMG">
                <span>Escoge una bandera</span>
                <div id="scrolltable">
                    <table>
                        <tr>
                            <th>
                                <div>Bandera</div>
                            </th>
                            <th>
                                <div>Seleccionar</div>
                            </th>
                        </tr>
                        <tr v-for="bandera in 196">
                            <td style="width: 10%"><img :src="`assets/img/paises/${bandera}.png`" width="25px"></td>
                            <td>
                                <button
                                    class="btn-floating btn-small waves-effect waves-light transparent modal-trigger"
                                    href="#paisForm"><i class="fa fa-check black-text"
                                        @click="pais.IMG=`assets/img/paises/${bandera}.png`"></i></button>
                            </td>
                        </tr>

                    </table>
                </div>
            </div>
            <div v-if="pais.IMG">
                <img :src="pais.IMG" width="25px">
                <br>
                <br>
                <br>
                <button class="btn transparent black-text btn-small" @click="pais.IMG = ''">Cambiar Bandera</button>
            </div>

        </div>
        <div class="modal-footer">
            <a v-if="!editar" class="modal-close waves-effect waves-green btn-flat green-text" @click="crearPais()"
                v-show="pais.NOMBRE && pais.IMG">Agregar</a>
            <a v-if="editar" class="modal-close waves-effect waves-green btn-flat green-text" @click="actualizarPais()"
                v-show="pais.NOMBRE && pais.IMG">Guardar cambios</a>
            <a class="modal-close waves-effect waves-green btn-flat">Cancelar</a>
        </div>
    </div>
</div>

<style>
    #scrolltable {
        margin-top: 20px;
        height: 200px;
        overflow: auto;
    }

    #scrolltable th div {
        position: absolute;
        margin-top: -20px;
    }
</style>
<script>
    document.addEventListener('DOMContentLoaded', function () {
        var elems = document.querySelectorAll('.modal');
        var instances = M.Modal.init(elems, {});
    });


    document.addEventListener('DOMContentLoaded', function () {
        var elems = document.querySelectorAll('.dropdown-trigger');
        var instances = M.Dropdown.init(elems, {});
    });


    let app = new Vue({
        el: "#app",
        data: {
            current_user_id: "<?=$this->session->userdata('ID')?>",
            paises: [],
            pais: {
                NOMBRE: '',
                IMG: ''
            },
            editar: false
        },
        created: function () {
            this.cargarPaises();
        },
        methods: {
            cargarPaises() {
                axios.get('index.php/api/table/paises').then(
                    result => this.paises = result.data,
                    error => console.log(error)
                )
            },
            crearPais() {
                axios.post('index.php/api/table/paises', {
                    data: this.pais
                }).then(
                    result => this.cargarPaises(),
                    error => console.log('Error al guardar el pais')
                )
            },
            borrarPais(pais) {
                if (confirm(`¿Estás seguro de eliminar '${pais.NOMBRE}'?`)) {
                    axios.delete('index.php/api/table/paises/' + pais.ID).then(
                        ok => this.cargarPaises(),
                        error => console.log('Ocurrio un error al eliminar el pais')
                    )
                }
            },
            limpiarPais() {
                this.editar = false;
                this.pais.NOMBRE = '';
                this.pais.IMG = '';
            },
            editarPais(pais) {
                this.editar = true;
                this.pais = Object.assign({}, pais);
            },
            actualizarPais() {
                let id = this.pais.ID;
                delete this.pais.ID;
                axios.put('index.php/api/table/paises/' + id, {
                    data: this.pais
                }).then(
                    ok => this.cargarPaises(),
                    error => console.log('Ocurrio un error al actualizar')
                )
            }
        }
    });
</script>