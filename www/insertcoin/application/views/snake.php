<div class="container-fluid" style="padding-left: 5%;padding-right: 5%" id="app">
    <div class="row animated bounceIn">

        <div class="card col s12  orange lighten-5">
            <div class="col s2 center " style="height: 57em">
                <div class="col s12" style="margin-top:15%">

                    <img :src="juego.IMG" width="50%" class="left">
                    <b v-if="juego.CLASIFICACION">{{juego.CLASIFICACION}}</b>
                    <br>
                    <small>Creado por: <a
                            :href="`index.php/inicio/usuario/${juego.CREADOR}`">{{juego.CREADOR}}</a></small>

                </div>
                <div class="col s12" >
                    <h4 class="left blue-grey-text">{{puntos}}</h4>

                </div>

                <div class="col s12  left-align" style="margin-bottom:5px; min-height: 15em">
                    <h6>Record <span class="right"><b>{{jugadoActual.RECORD}}</b></span></h6>
                    <h6>Promedio <span class="right"><b> {{jugadoActual.PROMEDIO}}</b></span></h6>
                </div>




               
                <div class="left-align">
                        <a  :href="`index.php/inicio/juego/${juego_id}/global`" class="btn black-text orange white-text">Clasificación Global</a>
                        <a style="margin-top:3%" :href="`index.php/inicio/juego/${juego_id}/stats/${current_user_id}`" class="btn black-text orange white-text">Mis estadísticas</a>
                    </div>
            </div>



            <div class="card-content col s8  orange lighten-5" style="height: 55em; border: 1px solid #ffb74d ">
                <div class="col s12 center">
                    <h4>Snake Game</h4>
                    <canvas id="snake" width="608" height="608"></canvas>
                </div>

            </div>
            <div class="col s2" style="height: 55em;">
                <h3 class="center  amber-text darken-3"><i class="fas fa-trophy"></i></h3>

                <table>
                    <thead>
                        <th style="width: 5%"></th>
                        <th style="width: 15%"></th>
                        <th>Clasificación</th>
                        <th style="width: 5%"></th>
                    </thead>
                    <tbody>
                        <tr v-for="(jugador,index) in jugadoActual.TOP">
                            <td>
                                {{index+1}}
                            </td>
                            <td>
                                <img :src="jugador.AVATAR" width="100%">
                            </td>
                            <td>
                                <a :href="`index.php/inicio/juego/${juego_id}/stats/${jugador.ID}`">{{jugador.USERNAME}}</a>
                            </td>
                            <td>
                                {{jugador.PUNTUACION}}
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>


        </div>


    </div>
</div>

<script>
    const app = new Vue({
        el: '#app',
        data: {
            current_user_id: "<?=$this->session->userdata('ID')?>",
            juego_id: "1",
            jugadoActual: {
                "AVATAR": "",
                "USERNAME": "",
                "IMG": "",
                "RECORD": '',
                "PROMEDIO": '',
                "ULTIMOSINTENTOS": [],
                "TOP": []
            },
            puntos: 0,
            nuevoRecord: false,
            juego: {}
        },
        created: function () {
            this.cargarDatosJugador();
            this.cargarDatosJuego();
        },
        methods: {
            cargarDatosJuego() {
                axios.get(`index.php/api/datosJuego/${this.juego_id}`).then(
                    response => {
                        this.juego = response.data;
                    }
                )
            },
            cargarDatosJugador() {
                axios.get(`index.php/api/jugadorActual/${this.current_user_id}/${this.juego_id}`).then(
                    response => {
                        this.jugadoActual = response.data;
                        //this.graficar1();
                    }
                )
            },
            terminarJuego() {
                this.jugando = false;
                //Verificar si es nuevo record
                console.log('HOLA');
                if (this.puntos > parseInt(this.jugadoActual.RECORD)) {
                    this.nuevoRecord = true;
                }

                //Guardar intento
                datos = {
                    juego_id: this.juego_id,
                    jugador_id: this.current_user_id,
                    puntuacion: this.puntos
                };

                axios.post('index.php/api/guardarIntento', datos).then(
                    response => this.cargarDatosJugador()
                );

            }
        }
    })

    const cvs = document.getElementById("snake");
    const ctx = cvs.getContext("2d");

    // create the unit
    const box = 32;

    // load images

    const ground = new Image();
    ground.src = "assets/ground.png";

    const foodImg = new Image();
    foodImg.src = "assets/food.png";

    // load audio files

    let dead = new Audio();
    let eat = new Audio();
    let up = new Audio();
    let right = new Audio();
    let left = new Audio();
    let down = new Audio();
    let coin = new Audio();

    dead.src = "assets/dead.mp3";
    eat.src = "assets/eat.mp3";
    up.src = "assets/up.mp3";
    right.src = "assets/right.mp3";
    left.src = "assets/left.mp3";
    down.src = "assets/down.mp3";
    coin.src = "assets/coin7.wav";


    // create the snake

    let snake = [];

    snake[0] = {
        x: 9 * box,
        y: 10 * box
    };

    // create the food

    let food = {
        x: Math.floor(Math.random() * 17 + 1) * box,
        y: Math.floor(Math.random() * 15 + 3) * box
    }

    // create the score var

    let score = 0;

    //control the snake

    let d;

    document.addEventListener("keydown", direction);

    function direction(event) {
        let key = event.keyCode;
        console.log(key);
        if (key == 37 && d != "RIGHT") {
            left.play();
            d = "LEFT";
        } else if (key == 38 && d != "DOWN") {
            d = "UP";
            up.play();
        } else if (key == 39 && d != "LEFT") {
            d = "RIGHT";
            right.play();
        } else if (key == 40 && d != "UP") {
            d = "DOWN";
            down.play();
        } else if (key == 13) {
            coin.play()
            app.puntos = 0;
            score = 0;
            clearInterval(game);
            d = '';
            snake = [];
            food.x = Math.floor(Math.random() * 17 + 1) * box;
            food.y = Math.floor(Math.random() * 15 + 3) * box;

            snake[0] = {
                x: 9 * box,
                y: 10 * box
            };
            game = setInterval(draw, 10);
        }
    }

    // cheack collision function
    function collision(head, array) {
        for (let i = 0; i < array.length; i++) {
            if (head.x == array[i].x && head.y == array[i].y) {

                return true;
            }
        }
        return false;
    }

    function nivel() {
        let n = snake.length;
        if (n < 13) {
            return n;
        }
        if (n < 35) {
            return 14;
        }
        return 15;
    }

    t = 0;


    // draw everything to the canvas
    //Tiempo
    function draw() {
        t += 1;
        console.log(t);
        if (t % (25 - nivel()) == 0) {
            t = 0
            ctx.drawImage(ground, 0, 0);

            for (let i = 0; i < snake.length; i++) {
                ctx.fillStyle = (i == 0) ? "green" : "white";
                ctx.fillRect(snake[i].x, snake[i].y, box, box);
                ctx.strokeStyle = "red";
                ctx.strokeRect(snake[i].x, snake[i].y, box, box);
            }

            ctx.drawImage(foodImg, food.x, food.y);

            // old head position
            let snakeX = snake[0].x;
            let snakeY = snake[0].y;

            // which direction
            if (d == "LEFT") snakeX -= box;
            if (d == "UP") snakeY -= box;
            if (d == "RIGHT") snakeX += box;
            if (d == "DOWN") snakeY += box;

            // if the snake eats the food
            if (snakeX == food.x && snakeY == food.y) {
                score++;
                app.puntos++;
                eat.play();
                food = {
                    x: Math.floor(Math.random() * 17 + 1) * box,
                    y: Math.floor(Math.random() * 15 + 3) * box
                }
                // we don't remove the tail
            } else {
                // remove the tail
                snake.pop();
            }

            // add new Head

            let newHead = {
                x: snakeX,
                y: snakeY
            }

            // game over

            if (snakeX < box || snakeX > 17 * box || snakeY < 3 * box || snakeY > 17 * box || collision(newHead,
                    snake)) {
                clearInterval(game);
                app.terminarJuego();
                dead.play();
            }

            snake.unshift(newHead);

            ctx.fillStyle = "white";
            ctx.font = "45px Changa one";
            ctx.fillText(score, 2 * box, 1.6 * box);
        }
    }

    // call draw function every 10 ms

    let game = setInterval(draw, 10);
</script>