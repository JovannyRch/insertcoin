<div class="container" id="app">
    <div class="row" v-if="!cargarUsuario">
        <h3 class="center orange-text">Mi perfil</h3>
        <div class="col s4">
            <div v-if="usuario.AVATAR" class="center">

            <img :src="usuario.AVATAR" width="100%" >
            <button class="btn transparent green-text" @click="usuario.AVATAR = ''">Cambiar avatar</button>
            </div>
            <div v-else>
                <img @click="usuario.AVATAR = `assets/img/avatars/${imagen}.png`" :src="`assets/img/avatars/${imagen}.png`"
                    class="col s3" v-for="imagen in 21">
            </div>

        </div>
        <div class="col s8">
            <h3 class="">{{usuario.USERNAME}}</h3>
            <div class="col s12">

                Fecha de registro: {{usuario.FECHA_REGISTRO}}
            </div>
            <div class="input-field col s6">
                <select v-model="usuario.PAIS_ID" class="browser-default" required>
                    <option>Elige tu país</option>
                    <option v-for="pais in paises" :value="pais.ID">{{pais.NOMBRE}}</option>>
                </select>
            </div>
            <div class="col s12">
                <img :src="usuario.IMG" width="20%">
            </div>
            <div class="col s12">
                <span>Mi biografía</span>
                <textarea v-model="usuario.BIO"></textarea>
            </div>
        </div>
        <div class="col s12 center">
            <button @click="guardarCambios" class="btn  orange white-text">Guardar Cambios</button>
        </div>
    </div>
    <div v-else class="center-align">
        <div class="progress">
            <div class="indeterminate"></div>
        </div>
    </div>
</div>

<script>
    document.addEventListener('DOMContentLoaded', function () {
        var elems = document.querySelectorAll('.modal');
        var instances = M.Modal.init(elems, {});
    });


    let app = new Vue({
        el: "#app",
        data: {
            current_user_id: "<?=$this->session->userdata('ID')?>",
            usuario: {
                AVATAR: '',
                BIO: '',
                ESTADO_ID: "",
                FECHA_NACIMIENTO: '',
                FECHA_REGISTRO: "",
                ID: "",
                PAIS_ID: '',
                USERNAME: "",
                "pais": {
                    "ID": "",
                    "NOMBRE": "",
                    "IMG": ""
                }
            },
            pais_id: '',
            cargarUsuario: true,
            paises: JSON.parse('<?=$paises?>')
        },
        created: function () {
            this.cargarPerfil();
        },
        methods: {
            cargarPerfil() {
                this.cargarUsuario = true;
                axios.get('index.php/api/miperfil/' + this.current_user_id).then(
                    usuario => {
                        this.usuario = usuario.data;
                        this.cargarUsuario = false;
                    },
                    error => {
                        console.log('Error al cargar el usuario');
                        this.cargarUsuario = false;
                    }
                )
            },
            guardarCambios(){
                data = {
                    BIO: this.usuario.BIO,
                    AVATAR: this.usuario.AVATAR,
                    PAIS_ID: this.usuario.PAIS_ID
                }

                if(!this.usuario.AVATAR){
                    alert('Selecciona un avatar')
                    return 
                }

                if(!this.usuario.BIO){
                    alert('Ingresa tu biografía')
                    return
                }

                axios.put('index.php/api/table/usuarios/'+this.current_user_id, {data: data}).then(
                    response => {
                        this.cargarPerfil()
                    }
                )
            }
        }
    });
</script>