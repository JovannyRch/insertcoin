<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">

    <base href="<?php echo $this->config->base_url()?>" />
    <link rel="stylesheet" href="assets/css/materialize.min.css">
    <link rel="shortcut icon" href="assets/img/icon.png" type="image/x-icon">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="assets/js/vue.js"></script>
    <script src="assets/js/axios.min.js"></script>
    <title>Login</title>

</head>

<body class="orange lighten-5">

    <div class="container" id="app" style="padding-top: 4%">

        <div class="card" style="margin: 0 auto; width: 80%">
            <div class="card-content">
                <div class="row">
                 <h2 class="center deep-orange-text">InsertCoin.com</h2>

                    <div class="col s4">

                        <div class="center-align" >
                            <img  :src="avatar? avatar: 'assets/img/registro.gif'" alt="Idlemania.com" class="card-image" width="100%"
                                style="padding-top: 25%"> <br>
                            
 
                        </div>

                    </div>
                    <div class="col s8">
                        
                        
                        <h3 class="card-title center-align"><b>Registro</b></h3>
                       
                        <div class="col s12">
                            <span ><b>Selecciona un avatar</b></span> <br>
                            <img @click="avatar = `assets/img/avatars/${imagen}.png`" :src="`assets/img/avatars/${imagen}.png`"  height="55px" v-for="imagen in 21">        
                        </div>
                        


                       <div class="col s12">
                            <span ><b>Nombre de usuario</b></span>
                            <input type="text" placeholder="Ingrese nombre de usuario" v-model="usuario">
                       </div>

                        <span><b>Contraseña</b></span>
                        <input type="password" placeholder="Ingrese su contraseña" v-model="pass">


                        <span><b>Confirme contraseña</b></span>
                        <input type="password" placeholder="Repita su contraseña" v-model="pass2">
                        
                        

                        <div class="input-field col s12">
                            <select v-model="pais_id" required>
                                <option value="" disabled selected>Elige tu país</option>
                                <option v-for="pais in paises" :value="pais.ID">{{pais.NOMBRE}}</option>>    
                            </select>
                        </div>

                        <span><b>Describenos un poco de ti ¿Qué te gusta? ¿Cúales son tus pasatiempos?</b></span>
                        <textarea v-model="bio" required >

                        </textarea>


                        <div class="col s12">
                            <div class="center-align" style="padding-top: 3%" v-if="validarCampos">
                                <button class="btn green " @click="registro">Registrarse</button>
    
                            </div>
                            <div v-else class="orange lighten-4 black-text center" style="padding: 2%">
                                {{error}}
                            </div>
                        </div>
                        <div class="right-align">
                            <a  href="index.php/login" class="white deep-orange-text" style="margin-top: 3%" >Iniciar sesión</a>
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </div>
    <script src="assets/js/materialize.min.js"></script>
    <script>
        const app = new Vue({
            el: '#app',
            data: {
                usuario: '',
                pass: '',
                pass2: '',
                paises: JSON.parse('<?=$paises?>'),
                error: '',
                pais_id: '',
                avatar: '',
                bio: ''
            },
            created: function () { 
                this.cargarPaises();
                document.addEventListener('DOMContentLoaded', function () {
                    var elems = document.querySelectorAll('select');
                    var instances = M.FormSelect.init(elems, {});
                });
            },
            methods: {
                registro() {
                    data = {
                        username: this.usuario,
                        pass: this.pass,
                        pais_id: this.pais_id,
                        avatar: this.avatar,
                        bio: this.bio
                    }
                    let url = "index.php/usuarios/register"
                    axios.post(url, data).then(
                        response => {
                            //Guardar sesion del usuario en localStorage
                            //localStorage.setItem('usuario') = response.data;
                            window.location.href = "index.php/inicio";                            
                            //console.log('ir al inicio');
                        },
                        error => {
                            this.username = '';
                            alert("El nombre de usuario ya exite, intenta con otro");
                        }
                    )
                },
                cargarPaises() {
                    let url = "index.php/api/table/paises";
                    axios.get(url).then(
                        response => {
                            this.paises = response.data;
                        },
                        error => console.log('Error al cargar los paises')

                    )
                }
            },
            computed: {
                validarCampos(){

                    if(!this.usuario || !this.pass || !this.pais_id || !this.bio){
                        this.error = "Llene todos los campos requeridos";
                        return false;
                    }
                    if(this.usuario.length > 0 && this.usuario.length < 3){
                        this.error = "El nombre de usuario tiene que al menos 3 caracteres";
                        return false;
                    }
                    if(this.pass.length > 0 && this.pass.length < 6){
                        this.error = "La constraseña debe tener al menos 6 caracteres"
                        return false;
                    }
                    if(this.pass !== this.pass2){
                        this.error = 'Las contraseñas no coinciden'
                        return false;
                    }
                    return true;

                }
            }
        })
    </script>

</body>

</html>