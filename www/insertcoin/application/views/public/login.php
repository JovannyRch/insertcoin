<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">

    <base href="<?php echo $this->config->base_url()?>" />
    <link rel="stylesheet" href="assets/css/materialize.min.css">
    <link rel="shortcut icon" href="assets/img/icon.png" type="image/x-icon">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="assets/js/vue.js"></script>
    <script src="assets/js/axios.min.js"></script>
    <title>Login</title>

</head>

<body class="orange lighten-5">

    <div class="container" id="app" style="padding-top: 5%">

        <div class="card" style="margin: 0 auto; width: 50%">
            <div class="card-content">
                <h2 class="center deep-orange-text">InsertCoin.com</h2>

                <h3 class="card-title center-align"><b>Inicio de sesión</b></h3>

                <div class="center-align">
                    <img src="assets/img/login.gif" alt="Idlemania.com" class="card-image" width="50%"> <br>
                </div>
                <form v-on:submit.prevent="login">

                    <span><b>Nombre del usuario</b></span>
                    <input type="text" placeholder="Escriba su nombre de usuario" v-model="usuario">

                    <span><b>Contraseña</b></span>
                    <input type="password" placeholder="Escriba su contraseña" v-model="pass">
                    <div class="col s12 red lighten-4 center" v-if="error" style="height:30px">
                        {{error}}
                    </div>
                    <div class="center-align" style="padding-top: 3%">
                        <button class="btn deep-orange" type="submit" >Iniciar sesión </button>

                    </div>
                </form>
                <div class="right-align">
                    <a href="index.php/registro" class="white deep-orange-text">Registrarse</a>
                </div>
            </div>

        </div>

    </div>

    <script>
        const app = new Vue({
            el: '#app',
            data: {
                usuario: '',
                pass: '',
                error: ''
            },
            methods: {
                login() {
                    data = {
                        username: this.usuario,
                        pass: this.pass
                    }
                    let url = "index.php/usuarios/login"
                    axios.post(url, data).then(
                        response => {
                            window.location.href = "index.php/inicio";
                        },
                        error => {
                            this.error = "Usuario no encontrado, verifique sus datos"
                        }
                    )
                }
            }
        })
    </script>

</body>

</html>