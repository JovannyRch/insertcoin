<div class="container-fluid" id="app" style="margin-left:3%">
    <div class="row" >

        <h4>Conversaciones</h4>
        <div class="col s6 m3">
            <ul class="collection" v-if="!cConversaciones">
                <li v-if="conversaciones.length" class="collection-item avatar" v-for="conversacion in conversaciones"
                    @click="abrirConversacion(conversacion.ID)">
                    <img :src="conversacion.PARTICIPANTE.AVATAR" alt="" class="circle">
                    <span class="title"><b>{{conversacion.PARTICIPANTE.USERNAME}}</b></span>
                    <br>
                    <i v-if="conversacion.ULTIMO_MENSAJE.AUTOR_ID === current_user_id">Tú: {{conversacion.ULTIMO_MENSAJE.MENSAJE}}</i>
                    <i v-else>{{conversacion.ULTIMO_MENSAJE.MENSAJE}}</i>

                    </p>
                    <div class="secondary-content black-text">
                        <small>{{conversacion.ULTIMA_ACTUALIZACION}}</small>
                    </div>

                </li>
                <div v-else class="center-align">
                    Aún no tienes conversaciones...
                </div>
            </ul>
            <div v-else class="center-align">
                    <div class="progress">
                        <div class="indeterminate"></div>
                    </div>
                </div>
        </div>
        <div class="col s6 m8">
            <div v-if="inicio" class="center-align">
                <i>Da click en una conversacion para ver los mensajes</i>
            </div>
            <div v-else>

                    <ul class="collection ">
                            <li class="collection-item avatar orange lighten-5">
                                <img :src="conversacionActual.PARTICIPANTE.AVATAR" alt="" class="circle">
                                <span class="title"><b><a :href="`index.php/inicio/usuario/${conversacionActual.PARTICIPANTE.USERNAME}`"> {{conversacionActual.PARTICIPANTE.USERNAME}}</a></b></span>
    
                                <a href="#!" class="secondary-content"> </a>
                            </li>
                        </ul>

                <div v-if="cMensajes">

                    <div class="progress">
                        <div class="indeterminate"></div>
                    </div>
                </div>
                <div v-else>

                    
                    <form class="col s12"  v-on:submit.prevent="enviarMensaje">
                            <input  class="col s11 materialize-textarea" placeholder="Escribe aquí tu mensaje" v-model="mensaje">
                            <button type="submit" class="col s1 btn waves-effect waves-light orange lighten-1 " >
                                    <i class="fas fa-paper-plane"></i>
                            </button>
                    </form>
                    <ul class="collection">
                        <li v-for="mensaje in conversacionActual.MENSAJES" :class="mensaje.AUTOR_ID == current_user_id ? 'collection-item  col s12  grey lighten-3' : 'collection-item col s12 right-align grey lighten-3'" style="padding: 1.5%; margin-top: 1px;">
                           <span :class="mensaje.AUTOR_ID == current_user_id ? ' orange lighten-5 rcorners2' : 'rcorners2  orange lighten-4'">  {{mensaje.MENSAJE}} </span>
                    
                        </li>
                    </ul>
                </div>
            </div>
        </div>

    </div>
   

</div>
<style>
.rcorners2 {
  border-radius: 25px;
  border: 1px solid #ffab40 ;
  padding: 1%;
}
</style>
<script>
    document.addEventListener('DOMContentLoaded', function () {
        var elems = document.querySelectorAll('.modal');
        var instances = M.Modal.init(elems, {});
    });


    let app = new Vue({
        el: "#app",
        data: {
            cConversaciones: true,
            cMensajes: false,
            current_user_id: "<?=$this->session->userdata('ID')?>",
            conversaciones: [],
            conversacionActual: [],
            inicio: true,
            mensaje: ''
        },
        created: function () {
            this.cargarConversaciones();
        },
        methods: {
            cargarConversaciones() {
                this.cConversaciones = true;
                axios.get('index.php/api/conversaciones/' + this.current_user_id).then(
                    conversaciones => {
                        this.conversaciones = conversaciones.data;
                        this.cConversaciones = false;
                    },
                    error => {
                        console.log('ocurrion un error al cargar las conversaciones');
                        this.cConversaciones = false;
                    }
                )
            },
            abrirConversacion(conversacion_id) {
                if (this.inicio) this.inicio = false;
                this.mensaje = '';
                this.cMensajes = true;
                axios.get(`index.php/api/conversacion/${conversacion_id}/${this.current_user_id}`).then(
                    ok => {
                        this.conversacionActual = ok.data;
                        this.cMensajes = false;
                    },
                    error => {
                        alert('Ocurrio un error al cargar los mensajes');
                        this.cMensajes = false;
                    }
                )

            },
            getClassMensaje(id){
                if(parseInt(this.current_user_id) == parseInt(id)){
                    return 'collection-item';
                }else{
                    return 'collection-item';
                }
            },
            enviarMensaje(){
                if(this.mensaje){
                    data = {
                        mensaje: this.mensaje,
                        emisor: this.current_user_id,
                        conversacion: this.conversacionActual.ID
                    }
                    this.mensaje = '';
                    axios.post('index.php/api/enviarMensajeConversacion',data).then(
                        ok => {
                            this.cargarConversaciones();
                            this.abrirConversacion(this.conversacionActual.ID);
                        },
                        error => alert('Ocurrió un error al enviar el mensaje')
                    )
                }
            }
        }
    });
</script>