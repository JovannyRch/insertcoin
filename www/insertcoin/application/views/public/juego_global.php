<div class="container-fluid" style="padding-left: 5%;padding-right: 5%; margin-top:1%;" id="app">
    <div class="row animated fadeIn orange lighten-5" v-show="!cargando">
        <div class="col s2" style="margin-top:5%;">
            <div class="col s12">

                <img :src="juego.IMG" width="50%" class="left">
                <b>{{juego.CLASIFICACION}}</b>
                <br>
                <small>Creado por: <br> <a :href="`index.php/inicio/usuario/${juego.CREADOR}`">{{juego.CREADOR}}</a></small>

            </div>
            <div style="margin-top:5%; margin-left: 8%" class="left">
                <a :href="`index.php/inicio/juego/${juego_id}`" class="btn orange white-text"
                    style="margin-top:10%">Jugar</a>
                    <br>
                    <a :href="`index.php/inicio/juego/${juego_id}/stats/${current_user_id}`" class="btn orange white-text"
                    style="margin-top:10%">Mis estadísticas</a>
            </div>
        </div>
        <div class=" col s10" style="min-height: 55em; ">
            <h4 class="center">Clasificación Global <i class="fas fa-globe-americas"></i></h4>
            <table>
                <thead>
                    <th style="width: 3%">#</th>
                    <th style="width: 2%"></th>
                    <th style="width: 10%">Jugador</th>

                    <th>Record</th>
                    <th>Promedio</th>
                    <th>Intentos</th>
                    <th style="width: 3%">Pais</th>

                </thead>
                <tbody>
                    <tr v-for="(jugador,index) in jugadores">
                        <td>{{index+1}}</td>
                        <td>
                            <img :src="jugador.AVATAR" width="100%">
                        </td>
                        <td>
                            <a :href="`index.php/inicio/juego/${juego_id}/stats/${jugador.ID}`">
                                {{jugador.USERNAME}}</a>
                        </td>
                        <td>
                            {{jugador.RECORD}}
                        </td>
                        <td>
                            {{fix(jugador.PROMEDIO)}}
                        </td>
                        <td>
                            {{jugador.INTENTOS}}
                        </td>

                        <td>
                            <img :src="jugador.PAIS" width="100%">
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="progress" v-show="cargando">
        <div class="indeterminate"></div>
    </div>
</div>
<script>
    let app = new Vue({
        el: "#app",
        data: {
            current_user_id: "<?=$this->session->userdata('ID')?>",
            juego_id: "<?=$juego_id?>",
            jugadores: [],
            cargando: false,
            pagina: 0,
            total: 0,
            juego: {}
        },
        created: function () {
            this.cargarEstadisticas();
        },
        methods: {
            cargarEstadisticas() {
                axios.get(`index.php/api/global/${this.juego_id}/${this.pagina}`).then(
                    response => {
                        this.jugadores = response.data.jugadores;
                        this.total = parseInt(response.data.total);
                        this.juego = response.data.juego;
                    }
                ).catch(error => {
                    alert("Error");
                });
            },
            fix(decimal) {
                return parseFloat(decimal).toFixed(2);
            }
        }
    });
</script>