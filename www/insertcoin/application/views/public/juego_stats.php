<div class="container-fluid" style="padding-left: 5%;padding-right: 5%;" id="app">
    <div class="row animated fadeIn" v-show="!cargando">
        <div class="card col s12  orange lighten-5" style="height: 55em; ">

            <div class="col s12 ">
                <div class="col s2" style="margin-top:5%;">
                    <h5 class="center" v-if="current_user_id === jugador_id">Mis estadísticas
                        <br> <b>{{datos.NOMBRE}} </b> <br>
                    </h5>
                    <h5 class="center" v-else>Estadísticas
                        <br> <b>{{datos.NOMBRE}} </b> <br>
                    </h5>
                    <img :src="datos.IMG" width="50%" class="left">
                    <b v-if="datos.CLASIFICACION">{{datos.CLASIFICACION}}</b>
                    <br>
                    <small>Creado por: <br> <a
                            :href="`index.php/inicio/usuario/${datos.CREADOR}`">{{datos.CREADOR}}</a></small>
                    <div class="col s12  left-align" style="margin-bottom:5px; height:15em;">
                        <br>
                        <br>
                        <h6>Record <span class="right"><b>{{datos.RECORD}}</b></span></h6>
                        <h6>Promedio <span class="right"><b> {{datos.PROMEDIO}}</b></span></h6>
                        <h6>Intentos <span class="right"><b> {{datos.INTENTOS}}</b></span></h6>
                    </div>
                    <div class="col s12">
                        <a :href="`index.php/inicio/juego/${juego_id}`" class="btn orange white-text">Jugar</a>
                        <a :href="`index.php/inicio/juego/${juego_id}/global`" class="btn orange white-text"
                            style="margin-top:3%">Clasificación Global</a>
                        <a :href="`index.php/inicio/usuario/${jugador.USERNAME}`" class="btn orange white-text"
                            style="margin-top:3%">Perfil del jugador</a>
                    </div>
                </div>
                <div class="col s8">
                    <div class="col s12">

                        <h5 class="center" v-else>Estadísticas
                            / <b>{{datos.NOMBRE}} </b> <br></h5>
                        <h4 class="center">
                            <img :src="jugador.AVATAR" width="5%">
                            <b>
                                {{jugador.USERNAME}}
                            </b>
                        </h4>
                    </div>
                    <div class="col s5">
                        <h5 class="center">Mejores puntuaciones</h5>
                        <table>
                            <thead>
                                <th>#</th>
                                <th>Puntuación</th>
                                <th>Fecha</th>
                            </thead>
                            <tbody>
                                <tr v-for="(p,index) in datos.MEJORESPUNTUACIONES">
                                    <td>
                                        {{index+1}}
                                    </td>
                                    <td>
                                        {{p.PUNTUACION}}
                                    </td>
                                    <td>
                                        {{p.FECHA}}
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="col s7" style="margin-top:5%">
                        <div>
                            <div id="grafica1" style="width: 50em">
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>
    <div class="progress" v-show="cargando">
        <div class="indeterminate"></div>
    </div>
</div>

<script>
    let app = new Vue({
        el: "#app",
        data: {
            current_user_id: "<?=$this->session->userdata('ID')?>",
            jugador_id: "<?=$jugador_id?>",
            juego_id: "<?=$juego_id?>",
            datos: {},
            cargando: false,
            pagina: 0,
            jugador: {}
        },
        created: function () {
            this.cargarEstadisticas();
            this.datosJugador();
        },
        methods: {
            cargarEstadisticas() {
                this.cargando = true;
                axios.get(`index.php/api/estadisticas/${this.juego_id}/${this.jugador_id}`).then(
                    response => {
                        this.cargando = false;
                        this.datos = response.data;
                        this.grafica1();
                    }
                ).catch(
                    error => {
                        this.cargando = false;
                    }
                )
            },
            datosJugador() {
                axios.get("index.php/api/table/usuarios/" + this.jugador_id).then(
                    response => {
                        this.jugador = response.data;
                    }
                ).catch(

                )
            },
            grafica1() {
                Highcharts.chart('grafica1', {

                    title: {
                        text: 'Últimos 5 intentos'
                    },

                    yAxis: {
                        title: {
                            text: 'Puntuación'
                        }
                    },
                    legend: {
                        layout: 'vertical',
                        align: 'right',
                        verticalAlign: 'middle'
                    },

                    plotOptions: {
                        series: {
                            label: {
                                connectorAllowed: true
                            },
                            pointStart: 1
                        }
                    },

                    series: [{
                        name: 'Puntuación',
                        data: app.getUltimasPuntuaciones()
                    }],


                    responsive: {
                        rules: [{
                            condition: {
                                maxWidth: 500
                            },
                            chartOptions: {
                                legend: {
                                    align: 'center',
                                    verticalAlign: 'bottom',
                                    layout: 'horizontal'
                                },
                                yAxis: {
                                    labels: {
                                        align: 'left',
                                        x: 0,
                                        y: -5
                                    },
                                    title: {
                                        text: null
                                    }
                                },
                                subtitle: {
                                    text: null
                                },
                                credits: {
                                    enabled: false
                                }
                            }
                        }]
                    }

                });
            },
            getUltimasPuntuaciones() {
                let resultado = [];
                for (p of this.datos.ULTIMOSINTENTOS) {
                    resultado.push(parseInt(p.PUNTUACION));
                }
                return resultado;
            }
        }
    });
</script>