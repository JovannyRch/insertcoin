<div class="container" id="app">
    <div class="row animated bounceIn" v-show="!cargando">


    </div>
    <div class="progress" v-show="cargando">
        <div class="indeterminate"></div>
    </div>

</div>

<script>
    document.addEventListener('DOMContentLoaded', function () {
        var elems = document.querySelectorAll('.modal');
        var instances = M.Modal.init(elems, {});
    });


    let app = new Vue({
        el: "#app",
        data: {
            current_user_id: "<?=$this->session->userdata('ID')?>",
            cargando: false
        },
        created: function () {

        },
        methods: {

        }
    });
</script>