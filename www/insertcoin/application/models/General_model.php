<?php

	class General_model extends CI_Model {

		public $unauthorized_tables = array(
			'usuarios'
		);

		public $rules = array(
			'usuarios' => array(
				'operations' => ['CREATE','READ','UPDATE', 'DELETE'],
				'READ' => array(
					'type' => 'sql',
					'sentence' => 'SELECT 
						usuarios.id,usuarios.username, usuarios.avatar,usuarios.avatar,paises.nombre,usuarios.bio,usuarios.pais_id,paises.img,usuarios.fecha_registro
					 	from usuarios inner join paises on
						 paises.id = usuarios.pais_id
					'
				)
			),
			'estados' => array(
				'operations' => ['CREATE','READ','UPDATE', 'DELETE'],
				'READ' => array(
					'type' => 'columns',
					'sentence' => 'id,nombre,pais_id'
				)
			) ,
			'jugadores' => array(
				'READ' => array(
					'operations' => ['CREATE','READ','UPDATE', 'DELETE'],
					'type' => 'sql',
					'sentence' => 'SELECT 
						usuarios.id,usuarios.username, usuarios.bio,paises.img,usuarios.fecha_registro,usuarios.avatar,paises.nombre
					 	from usuarios inner join paises on
						paises.id = usuarios.pais_id where tipo = 2
					'
				)
			),
			'jugador' => array(
				'READ' => array(
					'operations' => ['CREATE','READ','UPDATE', 'DELETE'],
					'type' => 'sql',
					'sentence' => 'SELECT 
						usuarios.id,usuarios.username,paises.img,usuarios.fecha_registro,usuarios.avatar,paises.nombre
					 	from usuarios inner join paises on
						paises.id = usuarios.pais_id
					'
				)
			),

			'administradores' => array(
				'READ' => array(
					'operations' => ['CREATE','READ','UPDATE', 'DELETE'],
					'type' => 'sql',
					'sentence' => 'SELECT 
						usuarios.id,usuarios.username,paises.img,usuarios.fecha_registro,usuarios.avatar,paises.nombre
					 	from usuarios inner join paises on
						paises.id = usuarios.pais_id where tipo = 1
					'
				)
			),
			'juegos' => array(
				'operations' => ['CREATE','READ','UPDATE','DELETE' ],
				'READ' => array(
					'type' => 'sql',
					'sentence' => 'SELECT 
						juegos.id,usuarios.username creador,juegos.creador_id,juegos.IMG,juegos.nombre,juegos.tipo, juegos.clasificacion_id
					 	from usuarios inner join juegos on
						juegos.creador_id = usuarios.id 
					'
				)
			),
			
			'juegos0' => array(
				'operations' => ['CREATE','READ','UPDATE','DELETE' ],
				'READ' => array(
					'type' => 'sql',
					'sentence' => 'SELECT 
						juegos.id,usuarios.username creador,juegos.creador_id,juegos.IMG,juegos.nombre,juegos.tipo
					 	from usuarios inner join juegos on
						juegos.creador_id = usuarios.id  where juegos.tipo = 0
					'
				)
				),
			'postadmin'=> array(
				'operations' => ['CREATE','READ','UPDATE','DELETE' ],
				'READ' => array(
					'type' => 'sql',
					'sentence' => 'SELECT 
						id,cuerpo,titulo,img,fecha from posts_admin order by fecha desc
					'
				)//Cargar la clasificacion
			),
			'clasificaciones2'=> array(
				'operations' => ['CREATE','READ','UPDATE','DELETE' ],
				'READ' => array(
					'type' => 'sql',
					'sentence' => 'SELECT 
						* from clasificaciones order by id asc
					'
				)//Cargar la clasificacion
			)

		);



		public function __construct(){
			parent::__construct();
			$this->load->database();
			
		}

		

		//CRUD 

		public function read($table,$id = null){
			if(array_key_exists ( $table , $this->rules )){

			///	if(!in_array('READ',$this->rules[$table]['operations'])){
			//		return array();
			//	}
				$sql = "";
				if($this->rules[$table]['READ']['type'] == "sql"){
					$sql = $this->rules[$table]['READ']['sentence'];
				}

				if($this->rules[$table]['READ']['type'] == "columns"){
					$columns = $this->rules[$table]['READ']['sentence'];
					$sql ="SELECT $columns from $table";
				}

				if(is_null($id)){
					return $this->db->query($sql)->result_array();
				}
				else{
					$sql= $sql."where $table.id = $id";
					return $this->db->query($sql)->row_array();
				}
			}else{
				if(is_null($id)){
					return $this->db->query("SELECT * from $table")->result_array();
				}
				else{
					$sql= "SELECT * from $table where $table.id = $id";
					return $this->db->query($sql)->row_array();
				}
			}
			
		}

		public function lastId($table){
			return $this->db->query("SELECT id from $table order by 1 desc")->row_array()['ID'];
		}
		
		public function create($table,$data){
			// Solo en ORACLE, en los demas gestores basta con $this->db->set($data)->insert($table);
			$sql = str_replace('"', '', $this->db->set($data)->get_compiled_insert($table));
			$id = $this->db->query($sql);
			if ($this->db->affected_rows() === 1) {
				return $this->read($table, $this->lastId($table));
			}else{
				return null;
			}
		}

		public function delete($table,$id){
			$query = "DELETE FROM $table where id = $id";
			$this->db->query($query);
		}

		public function update($table, $id,$data){	
			//  Para los demas gestores que no sean ORACLE 
			// $this->db->set($data)->where('id', $id)->update($table);
			$sql = str_replace('"', '', $this->db->set($data)->where('id', $id)->get_compiled_update($table));
			$this->db->query($sql);
			if ($this->db->affected_rows() === 1){
				return $this->read($table, $id);
			}else return null;
		}

		public function commit(){
			/*	$this->db->query("INSERT INTO continentes(nombre) values('América')");
			$this->db->query("INSERT INTO continentes(nombre) values('Europa')");
			$this->db->query("INSERT INTO continentes(nombre) values('África')");
			$this->db->query("INSERT INTO continentes(nombre) values('Asia')");
			$this->db->query("INSERT INTO continentes(nombre) values('Ocenanía')");
			$this->db->query("INSERT INTO continentes(nombre) values('Antártida')");
			assets/img/paises/184.png
*/
			return ':)';
		}

		//Guards
		public function isNotEnable($table){
			return in_array($table, $this->unauthorized_tables);
		}

		public function isOperationEnable($table, $operation){
			//return in_array($operation, $thi->rules
		}


		//Servicios

		public function estados($pais_id){
			return $this->db->query("SELECT id,nombre from estados where pais_id = $pais_id")->result_array();
		}

		//Obtiene todas las preguntas de un nivel
		public function preguntas($nivel){
			return $this->db->query("SELECT id,cuerpo,img,repuestas from preguntas where nivel_id = $nivel")->result_array();
		}

		//Obtiene los niveles de un juego con sus respectivas respuetas
		public function niveles($juego_id){
			$niveles = $this->db->query("SELECT id,nombre from niveles where juego_id = $juego_id")->result_array();
			$resultado = array();
			foreach ($niveles as $nivel) {
				
				$nivel['preguntas'] = $this->preguntas($nivel['ID']);
				$resultado[] = $nivel;
			}
			return $resultado;

		}

		//Obtiene las clasisficaciones por nivel
		public function nivelesXclasificacion($clasificacion){
			return $this->db->query("SELECT id,img,nombre from niveles where clasificacion_id = $clasificacion")->result_array();
		}

		public function clasificaciones(){
			return $this->db->query("SELECT * from clasificaciones")->result_array();
		}

		//Obtiene todos los juegos por clasificacion
		public function juegosClasificacion($clasificacion){
			return $this->db->query("SELECT 
			juegos.id,usuarios.username creador,juegos.nombre,juegos.tipo, juegos.img
			 from usuarios inner join juegos on
			juegos.creador_id = usuarios.id  and juegos.clasificacion_id = $clasificacion")->result_array();
	
			
		}

		//Obtener los niveles de un juego
		public function getNiveles($juego_id){
			return $this->db->query("SELECT id,nombre from niveles where juego_id = $juego_id order by 1")->result_array();
		}

		//Obtener la cantidad de preguntas por nivel
		public function getNivelesPreguntasRandom($juego_id){
			$niveles = $this->getNiveles($juego_id);
			
			$resultado = array();

			foreach ($niveles as $nivel) {
				$nivel_id = $nivel['ID'];
				$nivel['CANTIDAD'] = $this->db->query("SELECT count(id) from preguntas where nivel_id = $nivel_id")->row_array()['COUNT(ID)'];
				//La mitad de preguntas por nivel
				$cantidad = intval(intval($nivel['CANTIDAD'])/2);
				
				if(sizeof($niveles) == 1){
					$preguntas = $this->db->query("SELECT  *
				FROM    (
						SELECT  *
						FROM    preguntas where nivel_id = $nivel_id
						ORDER BY
								dbms_random.value
						)")->result_array();

					/*$preguntas = $this->db->query(" SELECT  *
						FROM    preguntas where nivel_id = $nivel_id ORDER BY id
						")->result_array();*/
				$nivel['PREGUNTAS'] = $preguntas;
				}else{
					$preguntas = $this->db->query("SELECT  *
				FROM    (
						SELECT  *
						FROM    preguntas where nivel_id = $nivel_id
						ORDER BY
								dbms_random.value
						)
				WHERE rownum <= 5")->result_array();
				$nivel['PREGUNTAS'] = $preguntas;
				}

				
				
				$resultado[] = $nivel;
			}
			return $resultado;
		}
		////SELECT * FROM preguntas SAMPLE(10) WHERE ROWNUM <= 20;

		public function getJuego($juego_id){
			$juego = $this->read('juegos',$juego_id);
			$clasificacion_id = $juego['CLASIFICACION_ID'];
			$juego['CLASIFICACION'] = $this->read('clasificaciones',$clasificacion_id)['NOMBRE'];
			$juego['NIVELES'] =  $this->getNiveles($juego_id);
			return $juego;
		}

		public function getJuego2($juego_id){
			$juego = $this->read('juegos',$juego_id);
			$clasificacion_id = $juego['CLASIFICACION_ID'];
			$juego['CLASIFICACION'] = $this->read('clasificaciones',$clasificacion_id)['NOMBRE'];
			$juego['NIVELES'] =  $this->getNivelesPreguntasRandom($juego_id);
			return $juego;
		}

		public function getPreguntasNivel($nive_id){
			return $this->db->query("SELECT id,cuerpo,img,respuestas from preguntas where nivel_id = $nive_id")->result_array();
		}

		public function comentariosPost1($post_id){
			return $this->db->query("SELECT c.id as comentario_id,u.username as usuario,u.avatar,c.contenido, c.autor_id,c.fecha from comentarios_post_admin c inner join usuarios u on u.id = c.autor_id where c.post_id = $post_id order by c.id desc")->result_array();
		}

		//Obtiene una publicacion de los administradores con sus comentarios
		public function post1full($post_id){
			return $this->db->query("SELECT 
			id,cuerpo,titulo,img,fecha from posts_admin where id = $post_id ")->row_array();
		}

		public function user_data($username){
			return $this->db->query(
				"SELECT id,bio,avatar,pais_id,bio,fecha_registro,fecha_nacimiento,username from usuarios where username = '$username'"
			)->row_array();
		}

		public function user_data2($id){
			return $this->db->query(
				"SELECT u.avatar,u.username,p.img from usuarios u inner join paises p on
				p.id = u.pais_id  where u.id = $id"
			)->row_array();
		}

		//Cargar las notas de  un usuario
		public function notas_usuario($usuario_id){
			return $this->db->query(
				"SELECT n.id,n.nota,n.fecha,n.autor_id,u.username,u.avatar from notas_usuario n inner join usuarios u on n.autor_id = u.id where n.usuario_id = $usuario_id order by n.id desc"
			)->result_array();
		}
		//Enviar mensaje a un uusario
		public function enviarMensaje($emisor,$receptor,$mensaje){
			$conversacion_id = $this->db->query("SELECT id from conversaciones where 
			id in (SELECT conversacion_id from participantes_conversacion where usuario_id = $emisor) and 
			id in (SELECT conversacion_id from participantes_conversacion where usuario_id = $receptor)
			and isGrupo = 0
			")->row_array()['ID'];

			//Si no existe una conversacion entre los dos integrantes, se crea dicha conversacion
			if(!$conversacion_id){
				//Se crea la conversacion
				//SECUENCIA2
				$this->db->query("INSERT into conversaciones(id,isGrupo) values(sec_conversaciones.nextval,0)");
				$conversacion_id = $this->db->query("select sec_conversaciones.currval from dual")->row_array()['CURRVAL'];

				//Agregar como participante a los dos usuarios
				$this->db->query("INSERT into participantes_conversacion(usuario_id,conversacion_id) values($emisor,$conversacion_id)");
				$this->db->query("INSERT into participantes_conversacion(usuario_id,conversacion_id) values($receptor,$conversacion_id)");

			}	

			//Agregar mensaje a la conversacion
			$data = array(
				'conversacion_id' => $conversacion_id,
				'autor_id' => $emisor,
				'mensaje' => $mensaje
			);	
			$mensaje = $this->create("mensajes",$data);
			//Del mensaje creado obtenemos su fecha para guardarlo como last_update en la conversacion
			$fecha = $mensaje['FECHA'];
			
			$dataConversacion = array(
				'ultima_actualizacion' => $fecha
			);

			$conversacion = $this->update("conversaciones", $conversacion_id,$dataConversacion);
			return $mensaje;
		}

		//Enviar mensaje a una conversacion
		public function enviarMensajeConversacion($emisor,$mensaje,$conversacion){
			if($mensaje){
				$data = array(
					'conversacion_id' => $conversacion,
					'autor_id' => $emisor,
					'mensaje' => $mensaje
				);	
				return $this->create("mensajes",$data);
			}
			return false;
		}

		//Obtiene los participantes de una conversacion
		public function participantesConversacion($conversacion_id){
			return $this->db->query("SELECT u.username,u.id,u.avatar from participantes_conversacion p inner join usuarios u on p.usuario_id = u.id where conversacion_id = $conversacion_id")->result_array();
		}

		//Obtiene los participantes de una conversacion
		public function participanteConversacion($conversacion_id,$participante_id){
			return $this->db->query("SELECT u.username,u.id,u.avatar from participantes_conversacion p inner join usuarios u on p.usuario_id = u.id where conversacion_id = $conversacion_id and u.id != $participante_id")->row_array();
		}

		//Obtiene el ultimo mensaje de una conversacion
		public function lastMessage($conversacion_id){
			return $this->db->query("SELECT autor_id,mensaje,fecha from mensajes where conversacion_id = $conversacion_id order by fecha desc")->row_array();
		}

		//Regresa todos los mensajes de una conversacion
		public function mensajes($conversacion_id){
			return $this->db->query("SELECT autor_id,mensaje,fecha from mensajes where conversacion_id = $conversacion_id order by fecha desc")->result_array();
		}

		//Carga las conversaciones de un usuario
		public function conversaciones($usuario_id){
			$conversaciones = $this->db->query("SELECT * from conversaciones where 
			id in (SELECT conversacion_id from participantes_conversacion where usuario_id = $usuario_id) order by ULTIMA_ACTUALIZACION desc
			")->result_array();
			$resultado = array();
			foreach ($conversaciones as $conversacion) {
				$conversacion_id = $conversacion["ID"];
				$isGrupo = $conversacion["ISGRUPO"];
				
				$conversacion['ULTIMO_MENSAJE'] = $this->lastMessage($conversacion_id);

				//Si no es grupo, cargar el nombre del usuario
				if($isGrupo == "0"){
					$conversacion['PARTICIPANTE'] = $this->participanteConversacion($conversacion_id,$usuario_id);
				}else{
					$conversacion['PARTICIPANTES'] =  $this->participantesConversacion($conversacion_id);
				}

				$resultado[] = $conversacion;
			}
			return $resultado;
	
	}

	public function conversacion($conversacion_id,$usuario_id){
		$conversacion = $this->db->query("SELECT * from conversaciones where id = $conversacion_id")->row_array();
		$isGrupo = $conversacion["ISGRUPO"];

		if($isGrupo == "0"){
			$conversacion['PARTICIPANTE'] = $this->participanteConversacion($conversacion_id,$usuario_id);
		}else{
			$conversacion['PARTICIPANTES'] =  $this->participantesConversacion($conversacion_id);
		}

		$conversacion['MENSAJES'] = $this->mensajes($conversacion_id);
		return $conversacion;
	}

	public function perfil($usuario_id){
		
	}

	public function preguntasXnivel($nivel_id){
		return $this->db->query("SELECT id,cuerpo,img,respuestas from preguntas where nivel_id = $nivel_id")->result_array();
	}

	public function juegosPlayer(){
		return $this->db->query("SELECT 
			j.id, j.img,j.nombre,c.nombre clasificacion, j.clasificacion_id, u.username,j.creador_id creador from 
			juegos j inner join clasificaciones c 
			on j.clasificacion_id = c.id
			inner join usuarios u 
			on u.id = j.creador_id
			where j.tipo = 1 order by j.clasificacion_id  
		")->result_array();
	}

	public function guardarResultado($pregunta_id,$respuesta){
		//Guarda un acierto
		if(intval($respuesta) == 1){
			$this->db->query("UPDATE preguntas_resultados set cant_resp_correctas =  cant_resp_correctas+1 where pregunta_id = $pregunta_id");
		}else{
			$this->db->query("UPDATE preguntas_resultados set cant_resp_incorrectas =  cant_resp_incorrectas+1 where pregunta_id = $pregunta_id");
		}
	}
	//Mejores puntuaciones por jugador en un juego
	public function getMejoresPuntuaciones($jugador_id,$juego_id){
		return $this->db->query("SELECT * from (SELECT puntuacion,TO_CHAR(fecha, 'DD-MM-YYYY') fecha from intentos where juego_id = $juego_id and jugador_id = $jugador_id
		order by puntuacion desc) consulta where ROWNUM < 11 
		")->result_array();
	}


	public function getRecord($jugador_id,$juego_id){
		return $this->db->query("SELECT record from records where juego_id = $juego_id and jugador_id = $jugador_id")->row_array()['RECORD'];
	}


	public function getPromedio($jugador_id,$juego_id){
		return $this->db->query("SELECT avg(puntuacion) from intentos where juego_id = $juego_id and  jugador_id = $jugador_id")->row_array()['AVG(PUNTUACION)'];
	}

	public function getUltimos5Intentos($jugador_id,$juego_id){
		return $this->db->query("SELECT * from (SELECT puntuacion from intentos where juego_id = $juego_id  and  jugador_id = $jugador_id  order by fecha desc) consulta where ROWNUM < 6")->result_array();
	}

	public function topPuntuaciones($juego_id){
		return $this->db->query("SELECT * from 
		(SELECT r.record puntuacion, u.id,u.username,u.avatar from records r 
		inner join 
		usuarios u on u.id = r.jugador_id and r.juego_id = $juego_id 
		order by puntuacion desc) consulta where ROWNUM < 11 ")->result_array();
	}
	/*
	public function topPuntuaciones($juego_id){
		$jugadores = $this->db->query("SELECT MAX(PUNTUACION) from intentos where jugador_id in (SELECT DISTINCT jugador_id  from intentos where juego_id = $juego_id)")->result_array();
	}*/

	public function getJugadorxJuego($jugador_id,$juego_id){
		//$this->actualizarRecord();
		$jugador = $this->user_data2($jugador_id);
		$jugador['RECORD'] = $this->getRecord($jugador_id,$juego_id);
		$jugador['PROMEDIO'] = number_format($this->getPromedio($jugador_id,$juego_id),2);
		$jugador['ULTIMOSINTENTOS'] = $this->getUltimos5Intentos($jugador_id,$juego_id);
		$jugador['TOP'] = $this->topPuntuaciones($juego_id);
		
		return $jugador;
	}

	public function datosJuego($juego_id){
		$juego = $this->read('juegos',$juego_id);
		$clasificacion_id = $juego['CLASIFICACION_ID'];
		if($clasificacion_id){
			$juego['CLASIFICACION'] = $this->read('clasificaciones',$clasificacion_id)['NOMBRE'];
		}
		return $juego;
	}

	public function guardarIntento($data){
		$p = $data['puntuacion'];
		$juego_id = $data['juego_id'];
		$jugador_id = $data['jugador_id'];
		//PROCEDIMIENTO
		$this->db->query("BEGIN crearIntento($jugador_id,$juego_id,$p);END;");
		$record = $this->getRecord($jugador_id,$juego_id);
		
		if(!is_null($record)){
			if(floatval($data['puntuacion']) > floatval($record)){
				//Actualizar el record
				$this->db->query("BEGIN actualizarRecord($jugador_id,$juego_id,$p);END;");
			}
		}else{
			//Si no existe ningun record, guardar el primer intento como record
			//PROCEDIMIENTO 
			$this->db->query("BEGIN crearRecord($jugador_id,$juego_id,$p);END;");
		}

		return true;
	}

	//Actualizar records
	public function actualizarRecord(){
		$juegos = $this->db->query("SELECT id from juegos")->result_array();
		foreach ($juegos as $juego) {
			$juego_id = $juego['ID'];
			$jugadores = $this->db->query("SELECT id from usuarios where id in (SELECT distinct jugador_id from intentos where juego_id = $juego_id)")->result_array();
			foreach ($jugadores as $jugador) {
				$jugador_id = $jugador["ID"];
				$record = $this->db->query("SELECT MAX(puntuacion) from intentos where jugador_id = $jugador_id and juego_id = $juego_id")->row_array()['MAX(PUNTUACION)'];
				$this->create('records',array('juego_id' => $juego_id 
				,'jugador_id' => $jugador_id,
				'record' => $record));
					
			}
		}
	}


	public function global($juego_id,$pagina){
		$li = 10*intval($pagina);
		$ls = ($li+10)+1;

		$juego = $this->read('juegos',$juego_id);
		$clasificacion_id = $juego['CLASIFICACION_ID'];
		if($clasificacion_id){
			$juego['CLASIFICACION'] = $this->read('clasificaciones',$clasificacion_id)['NOMBRE'];
		}
		//(SELECT avg(puntuacion) from intentos i where i.juego_id = $juego_id and clasificacion.id = i.jugador_id) promedio,
		//(SELECT count(*) from intentos i where i.juego_id = $juego_id and clasificacion.id = i.jugador_id) intentos
		
		$lista = $this->db->query("SELECT id,username,record,avatar,pais,
		 (select promedioJuego($juego_id,clasificacion.id) from dual) promedio,
		 (select intentosJuego($juego_id,clasificacion.id) from dual) intentos
		 from clasificacion where juego_id = $juego_id order by record desc 
		--OFFSET $li ROWS FETCH NEXT 10 ROWS ONLY
		")->result_array();
		$total = $this->db->query("SELECT count(*) total from clasificacion where juego_id = $juego_id")->row_array()['TOTAL'];
		return array(
			'jugadores' => $lista,
			'total' => $total,
			'juego' => $juego
		);
	}

	public function estadisticas($juego_id,$jugador_id){
		$estadisticas = $this->read('juegos',$juego_id);
		$clasificacion_id = $estadisticas['CLASIFICACION_ID'];
		if($clasificacion_id){
			$estadisticas['CLASIFICACION'] = $this->read('clasificaciones',$clasificacion_id)['NOMBRE'];
		}

		$estadisticas['RECORD'] = $this->getRecord($jugador_id,$juego_id);
		$estadisticas['PROMEDIO'] = number_format($this->getPromedio($jugador_id,$juego_id),2);
		$estadisticas['ULTIMOSINTENTOS'] = $this->getUltimos5Intentos($jugador_id,$juego_id);
		$estadisticas['MEJORESPUNTUACIONES'] = $this->getMejoresPuntuaciones($jugador_id,$juego_id);
		$estadisticas['INTENTOS'] = $this->db->query("SELECT count(*) intentos from intentos where jugador_id = $jugador_id and juego_id = $juego_id")->row_array()['INTENTOS'];
		return $estadisticas;
	}
	//Obtiene las veces jugado en los ultimos N dias
	public function getNdays($N){
		$dias = array();
		$cantidades = array();
		for($i = 0; $i < intval($N);$i++){
			$dia = $this->db->query("SELECT TO_CHAR(systimestamp - $i,'DD-MM-YYYY') dia from dual")->row_array()['DIA'];
			$cantidad = $this->db->query("SELECT count(*) cantidad from intentos where TO_CHAR(fecha,'DD-MM-YYYY') = '$dia'")->row_array()['CANTIDAD'];
			
			$dias[] = $dia;
			$cantidades[] = intval($cantidad);		
		}
		return array(
			'dias' => $dias,
			'cantidades' => $cantidades
		);
	}

	

	public function reporteGeneral(){
		$reporte['usuarios'] = $this->db->query("SELECT count(*) total from usuarios where tipo = 2")->row_array()["TOTAL"];
		$reporte['mensajes'] = $this->db->query("SELECT count(*) total from mensajes")->row_array()["TOTAL"];
		$reporte['juegos'] = $this->db->query("SELECT count(*) total from juegos")->row_array()["TOTAL"];
		$reporte['intentos'] = $this->db->query("SELECT count(*) total from intentos")->row_array()["TOTAL"];
		
		//GROUP BY
		//Cantidad de jugadores por pais
		$paises = $this->db->query("SELECT (SELECT nombre from paises where paises.id = usuarios.pais_id) pais,count(*) total from usuarios group by pais_id")->result_array();
		//Cantidad de juegos por clasificacion
		$juegos = $this->db->query("SELECT 
		(SELECT nombre from clasificaciones c where c.id = j.clasificacion_id) clasificacion,
		count(*) total from juegos j group by clasificacion_id
		")->result_array();


		$reporte['paises']['paises'] = $this->toArray($paises,'PAIS',0,"Sin Pais");
		$reporte['paises']['totales'] = $this->toArray($paises,'TOTAL',1);

		$reporte['clasificaciones']['nombres'] = $this->toArray($juegos,'CLASIFICACION',0,"Sin Nombre");
		$reporte['clasificaciones']['totales'] = $this->toArray($juegos,'TOTAL',1);


		//VISTAS
		$reporte['topJuegos'] = $this->db->query("SELECT * from juegosMasJugados")->result_array();
		$reporte['topActivos'] = $this->db->query("SELECT * from topJugadoresActivos")->result_array();

		$reporte["ultimosdias"] = $this->getNdays(15);
		return $reporte;
	}

	public function getJuegosClasificacion(){
		return $this->db->query("SELECT * from juegos_clasificacion")->result_array();
	}


	public function recordsPorJugador($jugador_id){
		return $this->db->query("SELECT j.id,j.nombre,j.img,r.record from records r inner join juegos j on 
			r.juego_id = j.id where r.jugador_id = $jugador_id order by r.record desc")->result_array();
	}

	
	public function toArray($data,$key,$tipo,$nulo = ""){
		$resultado = array();

		foreach ($data as $item) {
			if($tipo == 0) {
				if(is_null($item[$key]))$resultado[] = $nulo;
				else $resultado[] = $item[$key];
			}
			if($tipo == 1) $resultado[] = intval($item[$key]);
			if($tipo == 2) $resultado[] = floatval($item[$key]);
		}
		return $resultado;
	}

	public function log(){
		$registros = $this->db->query("SELECT * FROM DB_LOG order by fecha desc")->result_array();
		return $registros;
	}

	public function historial($pagina,$cantidad){
		$pagina = intval($pagina);
		$cantidad = intval($cantidad);

		$min = $pagina*$cantidad;
		$max = $pagina*$cantidad+$cantidad;

		$registros = $this->db->query("SELECT 
		* 
	from 
		( select 
			  ROWNUM rn, a.*
		  from 
			( SELECT (select username from usuarios where usuarios.id = intentos.JUGADOR_ID) jugador, 
			(select nombre from juegos where juegos.id = INTENTOS.JUEGO_ID) juego, puntuacion, fecha from intentos order by fecha desc ) a 
		  where 
			ROWNUM <= $max
		) 
	where 
		rn>= $min")->result_array();
		return $registros;
	}

	public function logs($pagina,$cantidad){
		$pagina = intval($pagina);
		$cantidad = intval($cantidad);

		$min = $pagina*$cantidad;
		$max = $pagina*$cantidad+$cantidad;

		$registros = $this->db->query("SELECT 
		* 
	from 
		( select 
			  ROWNUM rn, a.*
		  from 
			( SELECT * from db_log order by fecha desc ) a 
		  where 
			ROWNUM <= $max
		) 
	where 
		rn>= $min")->result_array();
		return $registros;
	}

	public function cantidadIntentos(){
		return $this->db->query("SELECT count(*) total from intentos")->row_array()['TOTAL'];
	}

	public function cantidadLogs(){
		return $this->db->query("SELECT count(*) total from db_log")->row_array()['TOTAL'];
	}

	//Uso de secuencias
	public function crearPublicacion($publicacion){
		$titulo = $publicacion['TITULO'];
		$cuerpo = $publicacion['CUERPO'];
		$img = $publicacion['IMG'];
		$query = "INSERT into posts_admin(id,titulo,cuerpo,img) 
		values(sec_publicaciones.nextval,'$titulo','$cuerpo','$img')";
		$this->db->query($query);

		return $true;
	}

	//Uso de sinonimos
	public function obtenerBitacoras($fecha1,$fecha2,$fecha3){
		
		if($fecha1){
			$bitacoras['desarrollador'] = $this->db->query("SELECT * from bitacora_desarrollador where TRUNC(fecha) = TO_DATE('$fecha1','YYYY-MM-DD')")->result_array();
			$bitacoras['publicista'] = $this->db->query("SELECT * from bitacora_publicista where TRUNC(fecha) = TO_DATE('$fecha1','YYYY-MM-DD')")->result_array();
			$bitacoras['verificador'] = $this->db->query("SELECT * from bitacora_verificador where TRUNC(fecha) = TO_DATE('$fecha1','YYYY-MM-DD')")->result_array();
			$bitacoras['administrador'] = $this->db->query("SELECT * from bitacora where TRUNC(fecha) = TO_DATE('$fecha1','YYYY-MM-DD')")->result_array();
		}else if($fecha2 and $fecha3){
			$bitacoras['desarrollador'] = $this->db->query("SELECT * from bitacora_desarrollador where TRUNC(fecha) >= TO_DATE('$fecha2','YYYY-MM-DD')  and TRUNC(fecha) <= TO_DATE('$fecha3','YYYY-MM-DD')")->result_array();
			$bitacoras['publicista'] = $this->db->query("SELECT * from bitacora_publicista where TRUNC(fecha) >= TO_DATE('$fecha2','YYYY-MM-DD')  and TRUNC(fecha) <= TO_DATE('$fecha3','YYYY-MM-DD')")->result_array();
			$bitacoras['verificador'] = $this->db->query("SELECT * from bitacora_verificador where TRUNC(fecha) >= TO_DATE('$fecha2','YYYY-MM-DD') and TRUNC(fecha) <= TO_DATE('$fecha3','YYYY-MM-DD')")->result_array();
			$bitacoras['administrador'] = $this->db->query("SELECT * from bitacora where TRUNC(fecha) >= TO_DATE('$fecha2','YYYY-MM-DD') and TRUNC(fecha) <= TO_DATE('$fecha3','YYYY-MM-DD')")->result_array();
		
		}
		else{
			$bitacoras['desarrollador'] = $this->db->query("SELECT * from bitacora_desarrollador ")->result_array();
			$bitacoras['publicista'] = $this->db->query("SELECT * from bitacora_publicista")->result_array();
			$bitacoras['verificador'] = $this->db->query("SELECT * from bitacora_verificador")->result_array();
			$bitacoras['administrador'] = $this->db->query("SELECT * from bitacora")->result_array();
		}
		return $bitacoras;
	}





}

	/*

		SELECT id from conversaciones where 
			id in (SELECT conversacion_id from participantes_conversacion where usuario_id = 4) and 
			id in (SELECT conversacion_id from participantes_conversacion where usuario_id = 6)
			and isGrupo = 0;

			SELECT *
			FROM preguntas
			WHERE ROWNUM < 3;

			SELECT i.puntuacion, u.username intentos i inner join usuarios u on u.id = i.jugador_id 
			WHERE  and i.juego_id = 45 order by puntuacion desc

		SELECT id, puntuacion
		FROM intentos
		ORDER BY puntuacion desc
		OFFSET 5 ROWS FETCH NEXT 5 ROWS ONLY;
	*/
?>
