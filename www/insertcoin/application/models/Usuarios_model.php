<?php

	class Usuarios_model extends CI_Model {

		public function __construct(){
			parent::__construct();
			$this->load->database();
		}

		//CRUD DE USUARIOS

		public function read($id){
			if(is_null($id)){
				$query="SELECT * from usuarios";

				$resultado = $this->db->query($query);
				return $resultado->result_array();
			}
			else{
				$query="SELECT * from usuarios where id = $id";
				$resultado = $this->db->query($query);
				return $resultado->row_array();
			}
			
		}

		public function create($usuario){
			$sql = str_replace('"', '', $this->db->set($usuario)->get_compiled_insert('usuarios'));
			$this->db->query($sql);
			if ($this->db->affected_rows() === 1) return true;
		}

		public function delete($id){
			$query = "DELETE FROM usuarios where id = $id";
			$this->db->query($query);
		}

		public function update($usuario, $id){
			$this->db->set($usuario)->where('id', $id)->update('usuarios');
			if ($this->db->affected_rows() === 1) return true;
		}

		public function isUsernameEnable($username){
			$resultado = $this->db->query("SELECT id from usuarios where username = '$username'");
			return $resultado->num_rows() == 0;
		}

		public function login($username, $pass){
			$pass = md5($pass);
			$resultado = $this->db->query("SELECT id,tipo,username from usuarios where username = '$username' and pass = '$pass'");
			if ($resultado->num_rows() > 0){
				return $resultado->row_array();
			}else return false;
		}

	}
?>
