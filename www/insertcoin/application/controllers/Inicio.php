<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inicio extends CI_Controller {


	public function __construct(){
		parent::__construct();
		$this->load->model('General_model');
	}
	

	public function index()
	{
		if($this->session->userdata('TIPO')){
			if($this->session->userdata('TIPO') == "2"){              
                $this->load->view('jugador/header');
                $this->load->view('jugador/inicio');
                $this->load->view('jugador/footer');
			}
			if($this->session->userdata('TIPO') == "1"){              
                $this->load->view('admin/header');
                $this->load->view('admin/inicio');
                $this->load->view('admin/footer');
			}
		}
		else{
    	    redirect("index.php/login");				  
		}
		}
		
		public function publicaciones(){
			if($this->session->userdata('TIPO')){
				if($this->session->userdata('TIPO') == "2"){              
									$this->load->view('jugador/header');
									$this->load->view('jugador/inicio');
									$this->load->view('jugador/footer');
				}
				if($this->session->userdata('TIPO') == "1"){              
									$this->load->view('admin/header');
									$this->load->view('admin/publicaciones');
									$this->load->view('admin/footer');
					}
			}
			else{
						redirect("index.php/login");				  
			}
		}

    public function publicacion($id)
    {
        $data['id'] = $id;
        if($this->session->userdata('TIPO')){
							if($this->session->userdata('TIPO') == "2"){              
												$this->load->view('jugador/header');
												$this->load->view('jugador/publicacion',$data);
												$this->load->view('jugador/footer');
							}
							if($this->session->userdata('TIPO') == "1"){              
												$this->load->view('admin/header');
												$this->load->view('admin/publicacion',$data);
												$this->load->view('admin/footer');
								}
						}
						else{
									redirect("index.php/login");				  
						}
				 }
		
		 
		 public function perfil(){
		$data['paises'] = json_encode($this->General_model->read('paises',null));

			if($this->session->userdata('TIPO')){
				if($this->session->userdata('TIPO') == "2"){              
									$this->load->view('jugador/header');
									$this->load->view('public/miperfil',$data);
									$this->load->view('jugador/footer');
				}
				if($this->session->userdata('TIPO') == "1"){              
									$this->load->view('admin/header');
									$this->load->view('public/miperfil',$data);
									$this->load->view('admin/footer');
					}
			}
			else{
						redirect("index.php/login");				  
			}
	 }

	 public function usuario($usuario){
		 
		if($this->session->userdata('TIPO') && $usuario){
			$data['usuario'] = $usuario;
			if($this->session->userdata('TIPO') == "2"){              
								$this->load->view('jugador/header');
								$this->load->view('jugador/usuario',$data);
								$this->load->view('jugador/footer');
			}
			if($this->session->userdata('TIPO') == "1"){              
								$this->load->view('admin/header');
								$this->load->view('admin/usuario',$data);
								$this->load->view('admin/footer');
				}
		}
		else{
					redirect("index.php/login");				  
		}
	 }

	 public function usuarios(){
		if($this->session->userdata('TIPO')){
			
			if($this->session->userdata('TIPO') == "1"){              
								$this->load->view('admin/header');
								$this->load->view('admin/usuarios');
								$this->load->view('admin/footer');
				}
		}
		else{
					redirect("index.php/login");				  
		}
	 }

	 public function paises(){
		if($this->session->userdata('TIPO')){
			
			if($this->session->userdata('TIPO') == "1"){              
								$this->load->view('admin/header');
								$this->load->view('admin/paises');
								$this->load->view('admin/footer');
				}

				
		}
		else{
					redirect("index.php/login");				  
		}
	 }

	 public function conversaciones(){
		if($this->session->userdata('TIPO')){
			
			if($this->session->userdata('TIPO') == "1"){              
					$this->load->view('admin/header');
					$this->load->view('public/conversaciones');
					$this->load->view('admin/footer');
				}

				if($this->session->userdata('TIPO') == "2"){              
					$this->load->view('jugador/header');
					$this->load->view('public/conversaciones');
					$this->load->view('jugador/footer');
				}
		}
		else{
				redirect("index.php/login");				  
		}
	 }

	 public function comunidad(){
		if($this->session->userdata('TIPO')){
			
			if($this->session->userdata('TIPO') == "2"){              
					$this->load->view('jugador/header');
					$this->load->view('jugador/comunidad');
					$this->load->view('jugador/footer');
				}

		}
		else{
				redirect("index.php/login");				  
		}
	 }

	 public function miperfil(){
		if($this->session->userdata('TIPO')){
			
			if($this->session->userdata('TIPO') == "1"){              
					$this->load->view('admin/header');
					$this->load->view('public/miperfil');
					$this->load->view('admin/footer');
				}

				if($this->session->userdata('TIPO') == "2"){              
					$this->load->view('jugador/header');
					$this->load->view('public/miperfil');
					$this->load->view('jugador/footer');
				}
		}
		else{
				redirect("index.php/login");				  
		}
	 }

	 public function juegos(){
		if($this->session->userdata('TIPO')){
			
			if($this->session->userdata('TIPO') == "1"){              
					$this->load->view('admin/header');
					$this->load->view('admin/clasificaciones');
					$this->load->view('admin/footer');
				}

				if($this->session->userdata('TIPO') == "2"){              
					$this->load->view('jugador/header');
					$this->load->view('jugador/juegos');
					$this->load->view('jugador/footer');
				}
		}
		else{
				redirect("index.php/login");				  
		}


		

	 }

	 public function juego($juego_id, $opcion = null,$jugador = null){
		if($this->session->userdata('TIPO')){
			
			$data['juego_id'] = $juego_id;
			if($this->session->userdata('TIPO') == "1"){              
					if($opcion == "stats"){
						$data['jugador_id'] = $jugador;

						$this->load->view('admin/header');
						$this->load->view('public/juego_stats',$data);
						$this->load->view('admin/footer');
					}else{
						if(intval($juego_id) == 81){
							$this->load->view('admin/header');
							$this->load->view('snake',$data);
							$this->load->view('admin/footer');
						}else{
								$this->load->view('admin/header');
								$this->load->view('admin/juego',$data);
								$this->load->view('admin/footer');
						}
					}
				}

				if($this->session->userdata('TIPO') == "2"){    
					
					if($opcion == "stats"){
						$data['jugador_id'] = $jugador;
						$this->load->view('jugador/header');
						$this->load->view('public/juego_stats',$data);
						$this->load->view('jugador/footer');
					}else if($opcion == "global"){
						$this->load->view('jugador/header');
						$this->load->view('public/juego_global',$data);
						$this->load->view('jugador/footer');
					}
					else {
						if(intval($juego_id) == 1){
							$this->load->view('jugador/header');
							$this->load->view('snake',$data);
							$this->load->view('jugador/footer');
						}else{
							$this->load->view('jugador/header');
							$this->load->view('jugador/juego',$data);
							$this->load->view('jugador/footer');
						}

					}
				}
		}
		else{
				redirect("index.php/login");				  
		}
	 }

		 
	 public function log(){
		$data['cantidad'] = $this->General_model->cantidadLogs();
		if($this->session->userdata('TIPO') == "1"){  
				$this->load->view('admin/header');
				$this->load->view('admin/log',$data);
				$this->load->view('admin/footer');	
		}
		
	 }

	 public function historial(){

		$data['cantidad'] = $this->General_model->cantidadIntentos();
		if($this->session->userdata('TIPO') == "1"){  
			$this->load->view('admin/header');
			$this->load->view('admin/historial',$data);
			$this->load->view('admin/footer');	
		}

	 }
	 
	
	 public function bitacoras(){

		if($this->session->userdata('TIPO') == "1"){  
			$this->load->view('admin/header');
			$this->load->view('admin/bitacoras');
			$this->load->view('admin/footer');	
		}

	 }

	 //docker commit 1ad413d99108 laboratoriobridge/oracle-12c-ee:versionInsertCoin
    
    
}
