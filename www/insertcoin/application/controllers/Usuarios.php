<?php
require(APPPATH.'libraries/REST_Controller.php');

class Usuarios extends REST_Controller{

    public function __construct(){
        parent::__construct();
        $this->load->model('Usuarios_model');
        $this->load->model('General_model');
        $this->load->library('session');
    }

    public function usuario_get($id = null){
      $respuesta = $this->Usuarios_model->read($id);
      if(isset($respuesta)){
          $this->response($respuesta, 200);
      }else{
          $this->response(null, 404);
      }
    }

    public function sql_get(){
        $respuesta = $this->Usuarios_model->commit();
      if(isset($respuesta)){
          $this->response($respuesta, 200);
      }else{
          $this->response(null, 404);
      }
    }

    public function register_post(){
        

        if($this->Usuarios_model->isUsernameEnable($this->post('username'))){
            $pass = $this->post('pass');
            $usuario = array(
                'username' => $this->post('username'),
                'pais_id' => $this->post('pais_id'),
                'avatar' => $this->post('avatar'),
                'bio' => $this->post('bio'),
                'pass' => md5($pass)
            );
            $this->General_model->create('usuarios',$usuario);
            $respuesta = $this->Usuarios_model->login($usuario['username'] ,$pass);

            //Iniciar sesion del usuario
            $session_usuario = $this->Usuarios_model->login($this->post('username'),$pass);
            $this->session->set_userdata($session_usuario);
            $this->response($respuesta, 200);
            
            
        }else{
            $this->response('El nombre del usuario ya existe', 401);
        }

       
    }

    public function login_post(){
        $username = $this->post('username');
        $pass = $this->post('pass');
        if($username and $pass){
            $respuesta = $this->Usuarios_model->login($username,$pass);
            if($respuesta){
                $newdata = $respuesta;
                $this->session->set_userdata($newdata);
                $this->response($this->session->userdata('TIPO'), 200);
                //$this->response($respuesta, 200);
            }else {
                $this->response("Usuario inválido", 404);
            }
        }
        $this->response("Campos incompletos", 404);
    }


    public function logout_get(){
        $this->session->sess_destroy();
		redirect('index.php/login');
    }



}
