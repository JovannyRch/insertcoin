<?php
require(APPPATH.'libraries/REST_Controller.php');

class Api extends REST_Controller{

    public function __construct(){
        parent::__construct();
        $this->load->model('General_model');
    }

    public function table_get($table,$id = null){
      $respuesta = $this->General_model->read($table,$id);
      if(isset($respuesta)){
          $this->response($respuesta, 200);
      }else{
          $this->response(null, 404);
      }
    }

    public function table_post($table){
        
        $respuesta = $this->General_model->create($table,$this->post('data'));
        if($respuesta){
            $this->response($respuesta, 201);
        }
        else {
            $this->response('Error al crear', 401);
        }
    }

    public function table_delete($table, $id){
        $this->General_model->delete($table,$id);
        $this->response('Borrado exitosamente', 201);
    }

    public function table_put($table,$id){
        $respuesta = $this->General_model->update($table,$id,$this->put('data'));
       if($respuesta)  $this->response($respuesta, 201);
       else  $this->response("Erro al actualizar en ".$table,400);
    }

    public function sql_get(){
        $respuesta = $this->General_model->commit();
      if(isset($respuesta)){
          $this->response($respuesta, 200);
      }else{
          $this->response(null, 404);
      }
    }


    
    public function estados_get($pais_id){
        $respuesta = $this->General_model->estados($pais_id);
      if(isset($respuesta)){
          $this->response($respuesta, 200);
      }else{
          $this->response(null, 404);
      }
    }

    public function niveles_get($juego_id){
        $respuesta = $this->General_model->niveles($juego_id);
      if(isset($respuesta)){
          $this->response($respuesta, 200);
      }else{
          $this->response(null, 404);
      }
    }

    public function clasificacionesNiveles_get(){
        $clasificaciones = $this->General_model->read('clasificaciones', null);
        $clasificacionConNiveles = array();
        foreach ($clasificaciones as $clasificacion ) {
            $clasificacion['niveles'] = $this->General_model->niveles($clasificacion['ID']);
            $clasificacionConNiveles[] =$clasificacion;
        }
        $this->response($clasificacionConNiveles, 200);
    }

    public function juegosXclasificaciones_get(){
        $clasificaciones = $this->General_model->read('clasificaciones2', null);
        $clasificacionConNiveles = array();
        foreach ($clasificaciones as $clasificacion) {
            $clasificacion['JUEGOS'] = $this->General_model->juegosClasificacion($clasificacion['ID']);
            $clasificacionConNiveles[] = $clasificacion;
        }
        $this->response($clasificacionConNiveles, 200);
    }

    //Clasificaciones con sus juegos
    public function clasificaciones_get(){
        
    }

    public function juegos_get($clasificacion_id){
        if($clasificacion_id){
            $juegos = $this->General_model->juegosClasificacion($clasificacion_id);
            $this->response($juegos, 200);
        }
    }


    public function juego_get($juego_id){
        $this->response($this->General_model->getJuego($juego_id), 200);
    }  


    
    public function juegoPlayer_get($juego_id){
        $this->response($this->General_model->getJuego2($juego_id), 200);
    }  

    //Obtiene las preguntas por nivel

    public function preguntasXnivel_get($nive_id){
        $this->response($this->General_model->getPreguntasNivel($nive_id),200);
    }


    //Obtiene una publicacion con sus comentarios
    public function post1_get($post_id){
        if($post_id){
            $post = $this->General_model->post1full($post_id);
            $post['comentarios'] = $this->General_model->comentariosPost1($post_id);
            $this->response($post,200);
        }
    }

    //Obtiene la informacion del usuario
    public function usuario_get($username){
        if($username){
            $usuario = $this->General_model->user_data($username);
            $usuario['pais'] = $this->General_model->read("paises",$usuario['PAIS_ID']);
            $usuario['notas'] = $this->General_model->notas_usuario($usuario['ID']);
            $this->response($usuario,200);
        }else{
            $this->response('Username vacio',404);
        }
    }

    //Enviar mensaje de un usuario a otro
    public function enviarMensaje_post(){
        $emisor = $this->post('emisor');
        $receptor = $this->post('receptor');
        $mensaje = $this->post('mensaje');
        $mensajeEnviado = $this->General_model->enviarMensaje($emisor,$receptor,$mensaje);
        if($mensajeEnviado){
            $this->response($mensajeEnviado,200);
        }else{
            $this->response('Error al enviar el mensaje',404);
        }

    }

    //Enviar mensaje a una conversacion
    public function enviarMensajeConversacion_post(){
        $emisor = $this->post('emisor');
        $mensaje = $this->post('mensaje');
        $conversacion = $this->post('conversacion');
        $mensajeEnviado = $this->General_model->enviarMensajeConversacion($emisor,$mensaje,$conversacion);
        if($mensajeEnviado){
            $this->response($mensajeEnviado,200);
        }else{
            $this->response('Error al enviar el mensaje',404);
        }
    }

    //Carga las conversaciones de un usuario
    public function conversaciones_get($usuario_id){
        //$usuario_id = $this->session->userdata('ID');
        if($usuario_id){
            $conversaciones = $this->General_model->conversaciones($usuario_id);
            $this->response($conversaciones,200);            
        }
        else{
            $this->response('Parametros incompletos',404);
        }
    }

    public function conversacion_get($conversacion_id,$usuario_id){
        if($conversacion_id){
            $conversacion = $this->General_model->conversacion($conversacion_id,$usuario_id);
            $this->response($conversacion,200);       
        }
        else{
            $this->response('Parametros incompletos',404);
        }
    }

    //Cargar el perfil de un usuario
    public function miperfil_get($usuario_id){
        if($usuario_id){
            $pefil = $this->General_model->read('usuarios',$usuario_id);
            //$pefil = $this->General_model->pefil($usuario_id);
            $this->response($pefil,200);       
        }
        else{
            $this->response('Parametros incompletos',404);
        }
    }


    //Carga las preguntas de un nivel
    public function preguntasNivel_get($nivel_id){
        $preguntas = $this->General_model->preguntasXnivel($nivel_id);
        $this->response($preguntas,200);      
    }

    //Carga todos los juegos para un jugador
    public function juegos_player_get(){
        $juegos = $this->General_model->juegosPlayer();
        $this->response($juegos,200);
    }

    //Guardar resultado de una pregunta
    public function guardarResultado_put($pregunta_id,$respuesta){
        $this->General_model->guardarResultado($pregunta_id,$respuesta);
        $this->response('OK',200);      
    }

    //Obtiene los datos de un jugador para un juego
    public function jugadorActual_get($jugador_id,$juego_id){
       $datos =  $this->General_model->getJugadorxJuego($jugador_id,$juego_id);
       $this->response($datos,200);    
    }

    //Guardar el puntaje obtenido en un intento
    public function guardarIntento_post(){
        $data = array(
            'juego_id'=> $this->post('juego_id'),
            'jugador_id'=> $this->post('jugador_id'),
            'puntuacion'=> $this->post('puntuacion')
        );
       $intento = $this->General_model->guardarIntento($data);
       $this->response($intento,200);   
    }

    //Obtiene los records de un jugador
    public function recordsJugador_get($jugador_id){
        $records = $this->General_model->recordsPorJugador($jugador_id);
        $this->response($records,200);   
    }

    //Obtiene la clasificacion global de un juego
    public function global_get($juego_id,$pagina){
        $clasificacion = $this->General_model->global($juego_id,$pagina);
        $this->response($clasificacion,200);
    }

    //Obtiene las estadisticas de un jugador en un juego
    public function estadisticas_get($juego_id,$jugador_id){
        $stats = $this->General_model->estadisticas($juego_id,$jugador_id);
        $this->response($stats,200);
    }

    //Obtiene los datos de un juego
    public function datosJuego_get($juego_id){
        $juego = $this->General_model->datosJuego($juego_id);
        $this->response($juego,200);
        
    }

    //Reporte general
    public function reporte_get(){
        $reporte = $this->General_model->reporteGeneral();
        $this->response($reporte,200);
    }

    //Obtiene todos los juegos con su clasificacion
    public function juegosClas_get(){
        $juegos = $this->General_model->getJuegosClasificacion();
        $this->response($juegos,200);
    }

    //Obtiene el historial de partidas en el sistem
    public function historial_get($pagina,$cantidad){
        $registros = $this->General_model->historial($pagina,$cantidad);
        $this->response($registros,200);
    }

    //Obtiene el historial de partidas en el sistem
    public function logs_get($pagina,$cantidad){
        $registros = $this->General_model->logs($pagina,$cantidad);
        $this->response($registros,200);
    }

    //Crear una publicacion
    public function publicacion_post(){
        $publicacion = $this->post('publicacion');
        $this->General_model->crearPublicacion($publicacion);
        $this->response("ok",200);
    }

    //Obtiene las bitacoras de todos los usuarios de la BD
    public function bitacoras_get(){
        $fecha1 = $this->get('fecha1');
        $fecha2 = $this->get('fecha2');
        $fecha3 = $this->get('fecha3');
        $bitacoras = $this->General_model->obtenerBitacoras($fecha1,$fecha2,$fecha3);
        
        $this->response($bitacoras,200);   
    }
}
