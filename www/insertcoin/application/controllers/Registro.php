<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Registro extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('General_model');
	}

	public function index()
	{
		$data['paises'] = json_encode($this->General_model->read('paises',null));
		$this->load->view('public/registro',$data);
  }
    
}
